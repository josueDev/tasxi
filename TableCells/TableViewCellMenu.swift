//
//  TableViewCellMenu.swift
//  TASXI
//
//  Created by Josué :D on 11/05/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit

class TableViewCellMenu: UITableViewCell {
    
    @IBOutlet var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
