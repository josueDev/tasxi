//
//  TableViewCellHistoryTravel.swift
//  TASXI
//
//  Created by Josué :D on 11/05/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit

class TableViewCellHistoryTravel: UITableViewCell {
    
    @IBOutlet var date: UILabel!
    @IBOutlet var car: UILabel!
    @IBOutlet var startTravel: UILabel!
    @IBOutlet var destinationTravel: UILabel!
    @IBOutlet var nameDriver: UILabel!
    @IBOutlet var starsPoints: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
