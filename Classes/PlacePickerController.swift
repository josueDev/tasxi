//
//  PlacePickerController.swift
//  PlacePicker-iOS
//
//  Created by Piotr Bernad on 05/07/2019.
//  Copyright © 2019 Piotr Bernad. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

public class PlacePickerController: UIViewController, PlacesDataSourceDelegate,CLLocationManagerDelegate {
    // Public
    public unowned var delegate: PlacesPickerDelegate?
    var mUserPosition : CLLocationCoordinate2D!
    var locationManager: CLLocationManager!
    var locValue:CLLocationCoordinate2D!
    var flagFirstAutoUserLocation = false
    
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
        
        if typeTextField == "finishTravel"
        {
            self.navigationItem.title = "Elija su destino"
        }
        else
        {
            self.navigationItem.title = "Elija su inicio"
        }
        
        if typeTextField == "favorito"
        {
            self.navigationItem.title = "Lugar favorito"
        }
        
        
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
        
        var bounds: CGRect = UIScreen.main.bounds
        var width:CGFloat = bounds.size.width
        var height:CGFloat = bounds.size.height
        
        let label = UILabel(frame: CGRect(x: 20, y: 100, width: width - 40, height: 55))
        label.text = "Toque el mapa para cambiar el pin de ubicación"
        label.textAlignment = .center
        label.backgroundColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.7)
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.layer.cornerRadius = 4.0
        label.layer.masksToBounds = true
        pickerView.mapView.addSubview(label)
        
    }
    

    
    public override func loadView() {
        self.view = PlacePickerView(frame: CGRect.zero)
        pickerView.mapView.delegate = self
        pickerView.mapView.isMyLocationEnabled = true
        config.pickerRenderer.configureMapView(mapView: pickerView.mapView)
        
        pickerView.tableView.delegate = self
        pickerView.tableView.dataSource = placesDataSource
        config.listRenderer.registerCells(tableView: pickerView.tableView)
        config.pickerRenderer.configureTableView(mapView: pickerView.tableView)
        placesDataSource.tableView = pickerView.tableView
        getLocation()
        //centerUser()
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        //setInitialCoordinateIfNeeded()
        //getLocation()
    }
    
    private func setInitialCoordinateIfNeeded() {
        if let initialCoordinate = self.config.initialCoordinate {
            let position = GMSCameraPosition(latitude: initialCoordinate.latitude, longitude: initialCoordinate.longitude, zoom: config.initialZoom)
            pickerView.mapView.animate(to: position)
        }
    }
    
    // Internal
    
    private var placesDataSource: PlacesDataSource!
    private var config: PlacePickerConfig!
    
    private var pickerView: PlacePickerView {
        return view as! PlacePickerView
    }
    
    private func setupNavigationBar() {
        let searchButton = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(showPlacesSearch))
        config.pickerRenderer.configureSearchButton(barButtonItem: searchButton)
        self.navigationItem.rightBarButtonItem = searchButton
        
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelSelection))
        config.pickerRenderer.configureCancelButton(barButtonItem: cancelButton)
        self.navigationItem.leftBarButtonItem = cancelButton
    }
    
    @objc
    private func cancelSelection() {
        self.delegate?.placePickerControllerDidCancel(controller: self)
    }
    
    private lazy var marker: GMSMarker = {
       return GMSMarker(position: CLLocationCoordinate2D(latitude: 0.0, longitude: 0.0))
    }()
    
    @objc
    private func showPlacesSearch() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = placesDataSource
        autocompleteController.placeFields = config.placeFields
        autocompleteController.autocompleteFilter = config.placesFilter
        
        present(autocompleteController, animated: true, completion: nil)
    }
    
    fileprivate func selectPlaceAt(coordinate: CLLocationCoordinate2D) {
        focusOn(coordinate: coordinate)
        
        let bounds = pickerView.mapView.cameraTargetBounds ??
            GMSCoordinateBounds(coordinate: pickerView.mapView.projection.visibleRegion().nearLeft,
                                coordinate: pickerView.mapView.projection.visibleRegion().farRight)
        
        placesDataSource.fetchPlacesFor(coordinate: coordinate, bounds:bounds)
    }
    
    private func focusOn(coordinate: CLLocationCoordinate2D) {
        marker.position = coordinate
        marker.map = pickerView.mapView
        let zoom = pickerView.mapView.camera.zoom >= 10 ? pickerView.mapView.camera.zoom : 10
        let position = GMSCameraPosition(target: coordinate, zoom: zoom)
        pickerView.mapView.animate(to: position)
    }
    
    internal func placePickerDidSelectPlace(place: GMSPlace) {
        delegate?.placePickerController(controller: self, didSelectPlace: place)
    }
    
    internal func autoCompleteControllerDidProvide(place: GMSPlace) {
        focusOn(coordinate: place.coordinate)
    }
    
    @objc func centerUser()
    {
        let loc : CLLocation = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        updateMapFrame(newLocation: loc, zoom: 15.0)
    }
    
    func getLocation(){
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.startUpdatingLocation()
    }
    
    
}

extension PlacePickerController: GMSMapViewDelegate {
    public func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        selectPlaceAt(coordinate: coordinate)
    }
    
    public func mapView(_ mapView: GMSMapView, didTapPOIWithPlaceID placeID: String, name: String, location: CLLocationCoordinate2D) {
        marker.position = location
        marker.map = pickerView.mapView
        pickerView.mapView.animate(toLocation: location)
        placesDataSource.fetchPlaceDetails(placeId: placeID)
    }
    

    
    func updateMapFrame(newLocation: CLLocation, zoom: Float) {
        UIView.animate(withDuration: 2.0, animations: {
            let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
            self.pickerView.mapView.animate(to: camera)

        })
 
    }
    

  
}

extension PlacePickerController: UITableViewDelegate {
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        placesDataSource.didSelectListItemAt(index: indexPath.row)
    }
    
    
}

public extension PlacePickerController {
    static func controler(config: PlacePickerConfig) -> PlacePickerController {
        let controller = PlacePickerController()
        controller.config = config
        controller.placesDataSource = PlacesDataSource(renderer: config.listRenderer)
        controller.placesDataSource.delegate = controller
        return controller
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        locValue = manager.location!.coordinate
        let loc : CLLocation = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        if (flagFirstAutoUserLocation == false)
        {
            selectPlaceAt(coordinate: locValue)
            focusOn(coordinate: locValue)
            updateMapFrame(newLocation: loc, zoom: 15.0)
            flagFirstAutoUserLocation = true
        }
        
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    

}
