//
//  MapViewController.swift
//  TASXI
//
//  Created by Josué :D on 10/05/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
import LSDialogViewController

import Alamofire
import SwiftyJSON
import PKHUD

var timer = Timer()
var loadingAlertController = UIAlertController()
var mapViewControllerGlobal : MapViewController? = nil
var flagDialogEvaluateDriver = false
var typeTextField = ""

class MapViewController: UIViewController, GMSMapViewDelegate,CLLocationManagerDelegate,UITextFieldDelegate,MapPathViewModelDelegate {

    @IBOutlet var callTaxi: UIButton!
    @IBOutlet var finishTravel: UITextField!
    @IBOutlet var initTravel: UITextField!
    
    @IBOutlet var buttonInitTravel: UIButton!
    @IBOutlet var buttonFinishTravel: UIButton!
    
    @IBOutlet var mapView: GMSMapView!
    
    @IBOutlet var viewProfileDriver: UIView!
    @IBOutlet var bottomMap: NSLayoutConstraint!
    
    @IBOutlet var labelPriceTravel: UILabel!
    
    @IBOutlet var buttonCenter: UIButton!
    @IBOutlet var buttonPlaceInit: UIButton!
    
    @IBOutlet var buttonPlaceFinish: UIButton!
    
    @IBOutlet var buttonCall: UIButton!
    
    
    var mUserPosition : CLLocationCoordinate2D!
    var locationManager: CLLocationManager!
    var locValue:CLLocationCoordinate2D!
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var latitudeInit : Double = 0.0 , longitudeInit: Double = 0.0, latitudeFinish : Double = 0.0, longitudeFinish : Double = 0.0
    
    
    var objMapModel = MapPathViewModel()
    var iTemp:Int = 0
    var marker = GMSMarker()
    
    var latitudeDriver : Double = 0.0, longitudeDriver : Double = 0.0, angleDriver = 0.0
    
    var flagFinishTimer = true
    var flagUpdateInitTravel = false
    var flagStatus = ""
    var flagAutoCancel = false
    var minutesTravel = ""
    var kilometersTravel = ""
    var priceAprox = "0.0"
    var travelPreferentialtake = false
    var counterTimer = 0
    
    
    //inially load location on map
    //let camera: GMSCameraPosition = GMSCameraPosition.camera(withLatitude: 22.857165, longitude: 77.354613, zoom: 4.0)
    
    var dialogViewController = CustomDialogViewController()

    var markers = [GMSMarker]()
    
    
    var flagFirstAutoUserLocation = false
    var pickTabla : PickTabla!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
        self.navigationItem.title = "INICIO"
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
        
        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "menubar.png"), style: .done, target: self, action: #selector(showMenu))
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        UIApplication.shared.statusBarStyle = .default
        //navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(showMenu))
        
        buttonPlaceInit.imageView?.contentMode = .scaleAspectFit
        buttonPlaceFinish.imageView?.contentMode = .scaleAspectFit
        getLocation()
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        
        mapViewControllerGlobal = self
        
        initTravel.delegate = self
        finishTravel.delegate = self
        
        viewProfileDriver.isHidden = true
        
        //addPullUpController()
        //mCloseToYouPresenter.loadBranchOffice()
        
        //let camera = GMSCameraPosition.camera(withLatitude: (19.3843749), longitude: (-99.1778559), zoom: 11.0)
        //mapView.animate(to: camera)
        
        callTaxi.addTarget(self, action: #selector(self.pushInitTravel) , for: UIControlEvents.touchUpInside)
        
        buttonCenter.addTarget(self, action: #selector(self.centerUser) , for: UIControlEvents.touchUpInside)
        buttonCall.addTarget(self, action: #selector(self.callCenter) , for: UIControlEvents.touchUpInside)
        
        finishTravel.inputView = UIView()
        initTravel.inputView = UIView()
        //pageSetUp()
        checkTravelIncomplete()

        
        pendingRatingTrip(arg: false, completion: { (success) -> Void in
            if success { // this will be equal to whatever value is set in this method call
                print("true pendingRatingTrip")
                self.showDialog(.fadeInOut)
            } else {
                print("false pendingRatingTrip")
            }
        })
        
        labelPriceTravel.text = "Elija sus lugares de inicio y de destino para calcular su tarifa de viaje"
        labelPriceTravel.backgroundColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0.7)
        labelPriceTravel.numberOfLines = 2
        labelPriceTravel.adjustsFontSizeToFitWidth = true
        labelPriceTravel.layer.cornerRadius = 4.0
        labelPriceTravel.layer.masksToBounds = true
        
        //pickTabla = PickTabla(datos: ["LAND","TENANT"])
        
        buttonInitTravel.addTarget(self, action: #selector(self.initializeInitPickerGoogle), for: UIControlEvents.touchUpInside)
        
        buttonFinishTravel.addTarget(self, action: #selector(self.initializeFinishPickerGoogle), for: UIControlEvents.touchUpInside)
        buttonInitTravel.setTitle("", for: UIControlState.normal)
        buttonFinishTravel.setTitle("", for: UIControlState.normal)
        
        buttonPlaceInit.addTarget(self, action: #selector(self.addFavoriteInit), for: UIControlEvents.touchUpInside)
        buttonPlaceFinish.addTarget(self, action: #selector(self.addFavoriteFinish), for: UIControlEvents.touchUpInside)
        
        PlacePicker.configure(googleMapsAPIKey: llaveMapa, placesAPIKey: llaveMapa)
        
        
       // pickTabla.reloadData()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        pickTabla.showPickerr(ancho: self.finishTravel.frame.width , viewcontroller: self, x: finishTravel.frame.origin.x, y: finishTravel.frame.origin.y, boton: buttonFinishTravel)
    }
    
    fileprivate func showDialog(_ animationPattern: LSAnimationPattern) {
        
        if (flagDialogEvaluateDriver == false)
        {
        dialogViewController = CustomDialogViewController(nibName: "CustomDialog", bundle: nil)
        dialogViewController.delegate = self
        self.presentDialogViewController(
            dialogViewController,
            animationPattern: animationPattern,
            backgroundViewType: LSDialogBackgroundViewType.solid,
            dismissButtonEnabled: false,
            completion: nil)
        }
    }
    
    @objc func callCenter()
    {
        DispatchQueue.main.async {
            
            HUD.show(.progress)
            
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            
            let parameters: Parameters = [
                "id_viaje" : ""
            ]
            
            print("WS_TEL")
            print("\(ApiDefinition.WS_TEL)")
            
            Alamofire.request(ApiDefinition.WS_TEL, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("WS_TEL: \(json)")
                    let resultado = json["telefono"]
                    let res = resultado[0]["telefono"]
                    if res != 0
                    {
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            
                            
                            if let url = URL(string: "tel://\(res)"), UIApplication.shared.canOpenURL(url) {
                                if #available(iOS 10, *) {
                                    UIApplication.shared.open(url)
                                } else {
                                    UIApplication.shared.openURL(url)
                                }
                            }
                            
                            
                        }
                    }
                    else
                    {
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            
                        }
                    }
                case .failure(let error):
                    print(error)
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        
                    }
                }
            }
        }
    }
    
    func dismissDialog() {
        self.dismissDialogViewController(LSAnimationPattern.fadeInOut)
    }
    
    @objc func centerUser()
    {
        let loc : CLLocation = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        updateMapFrame(newLocation: loc, zoom: 15.0)
    }
    
    
    @objc func pushInitTravel()
    {
        DispatchQueue.main.async {
            let status_viaje = String(describing: UserDefaults.standard.value(forKey: DataPersistent.status_viaje)!)
            print("VIAJE PUSH BUTTTON \(status_viaje)")
            switch status_viaje {
            case "0":
                if ((self.initTravel.text != "") && (self.finishTravel.text != ""))
                {
                    //self.requestTrip()
                    self.requestTrip()
                    
                    
                    //            viewProfileDriver.isHidden = false
                    //
                    //            //drawRoute(latitudeInit: latitudeInit, longitudeInit: longitudeInit, latitudeFinish: latitudeFinish, longitudeFinish: longitudeFinish)
                    //
                    //            objMapModel.arrayMapPath.removeAll()
                    //            pageSetUp() // demo
                    //
                    //            timer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true, block: { (_) in
                    //                self.playCar()
                    //            })
                    //            //buttonPlay.isEnabled = false
                    //            RunLoop.current.add(timer, forMode: RunLoopMode.commonModes)
                    //
                    //            UIView.animate(withDuration: 0.5, animations: {
                    //                self.bottomMap.constant = -150 //
                    //                self.view.layoutIfNeeded()
                    //            })
                    
                }
                else
                {
                    let alert = UIAlertController(title: "Taxsi", message: "Por favor indique los lugares de partida y de destino", preferredStyle: .alert)
                    let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                    alert.addAction(actionOK)
                    self.present(alert, animated: true, completion: nil)
                }
                
            default:
                print("Error status_viaje")
            }
        }
        
    }
    
    func timerOn()
    {
        
        if counterTimer == 0
        {
            print("TIMER ON")
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (_) in
                self.counterTimer = self.counterTimer + 1
                self.getCoordDriver()
                
            })
            //buttonPlay.isEnabled = false
            
            RunLoop.current.add(timer, forMode: RunLoopMode.commonModes)
        }
        else
         {
               print("TIMER IN PROGRESS")
        }
            
        
        
     
        

    }
    
    func getPlacePickerView() {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
    
    func addMarkerInMap(lat: Double , long: Double, title: String, snippet : String){
        
        for (index, _) in markers.enumerated() {
            let marker : GMSMarker = markers[index]
            
            print("Item \(index): \(marker.title)")
            if title == marker.title
            {
                self.markers[index].map = nil
            }
            //self.markers[index].map = nil
        }
        let marker = GMSMarker()
        mUserPosition = CLLocationCoordinate2D(latitude: (lat), longitude: (long))
        marker.position = mUserPosition
        marker.title = title
        marker.snippet = snippet
        marker.icon = UIImage(named: "ic_marker")
        marker.map = mapView
        
        markers.append(marker)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print(marker.title!)
        print(marker.snippet!)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        locValue = manager.location!.coordinate
        let loc : CLLocation = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        if (flagFirstAutoUserLocation == false)
        {
            updateMapFrame(newLocation: loc, zoom: 12.0)
            flagFirstAutoUserLocation = true
        }
        
        //print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func getLocation(){
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.startUpdatingLocation()
    }
    
    @objc func showMenu()
    {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.navigationController?.view.layer.add(transition, forKey: nil)
        //_ = self.navigationController?.popToRootViewController(animated: false)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let pushController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        self.navigationController?.pushViewController(pushController, animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //let acController = GMSAutocompleteViewController()
        //acController.delegate = self as! GMSAutocompleteViewControllerDelegate
        //self.present(acController, animated: true, completion: nil)
        //self.getPlac@objc ePickerView()
    }
    
    @objc func addFavoriteInit()
    {
         let status = String(describing: UserDefaults.standard.value(forKey: DataPersistent.status_viaje)!)
        if (status == "0")
        {
        typeTextField = "initTravel"
        self.getFavorites()
        }
    }
    
    @objc func addFavoriteFinish()
    {
         let status = String(describing: UserDefaults.standard.value(forKey: DataPersistent.status_viaje)!)
        if (status == "0")
        {
        typeTextField = "finishTravel"
        self.getFavorites()
        }
        //self.addFavoriteTextField()
    }
    
    @objc func addFavoriteTextField()
    {
        var values = ["", ""]
        values.removeAll()
        for objeto in ObjectFavorites.getData()
        {
            let place :  ObjectFavorites = objeto as! ObjectFavorites
            let placeFavorite = place.nombre_lugar
            values.append(placeFavorite)
        }
        
        let firstPlace : ObjectFavorites = ObjectFavorites.getData().object(at: 0) as! ObjectFavorites

        DPPickerManager.shared.showPicker(title: "Mis favoritos", selected: firstPlace.nombre_lugar, strings: values) { (value, index, cancel) in
            if !cancel {
                // TODO: you code here
                //debugPrint(value as Any)
                //self.fieldGender.text = value
                print(index)
                self.selectPlaceFavorite(index: index)
            }
        }
        

    }
    
    func selectPlaceFavorite(index : Int)
    {
        let object : ObjectFavorites = ObjectFavorites.getData().object(at: index) as! ObjectFavorites
        if typeTextField == "finishTravel"
        {
            
            
            finishTravel.text = object.nombre_lugar
            
            latitudeFinish = Double(object.latitud)!
            longitudeFinish = Double(object.longitud)!
            addMarkerInMap(lat: latitudeFinish, long: longitudeFinish, title: "Destino", snippet: "")
            let loc : CLLocation = CLLocation(latitude: latitudeFinish, longitude: longitudeFinish)
            updateMapFrame(newLocation: loc, zoom: 15.0)
        }
        else
        {
            initTravel.text = object.nombre_lugar
            
            latitudeInit = Double(object.latitud)!
            longitudeInit = Double(object.longitud)!
            addMarkerInMap(lat: latitudeInit, long: longitudeInit, title: "Inicio", snippet: "")
            let loc : CLLocation = CLLocation(latitude: latitudeInit, longitude: longitudeInit)
            updateMapFrame(newLocation: loc, zoom: 15.0)
        }
        
        if ((initTravel.text != "") && (finishTravel.text != ""))
        {
            labelPriceTravel.text = "Calculando datos de viaje"
            DispatchQueue.main.async {
                self.calculatePriceRoute(latitudeInit: self.latitudeInit, longitudeInit: self.longitudeInit, latitudeFinish: self.latitudeFinish, longitudeFinish: self.longitudeFinish)
            }
        }
        
    }
    
    @objc func initializeInitPickerGoogle()
    {
        typeTextField = "initTravel"
        //self.getPlacePickerView()
        getPlacePickerViewNew()
    }
    
    @objc func initializeFinishPickerGoogle()
    {
        typeTextField = "finishTravel"
        //self.getPlacePickerView()
        getPlacePickerViewNew()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == finishTravel {
            typeTextField = "finishTravel"
            return true; //do not show keyboard nor cursor
        }
        if textField == initTravel {
            typeTextField = "initTravel"
            return true; //do not show keyboard nor cursor
        }
        typeTextField = ""
        
        return true
    }
    
    func getPlacePickerViewNew()
    {
        let controller = PlacePicker.placePickerController()
        controller.delegate = self
        let navigationController = UINavigationController(rootViewController: controller)
        self.show(navigationController, sender: nil)
    }
}

extension MapViewController : GMSPlacePickerViewControllerDelegate
{
    // GMSPlacePickerViewControllerDelegate and implement this code.

    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        //self.viewContainer.isHidden = false
        //self.indicatorView.isHidden = true
        //viewController.title = "Easkj kjs"
        //viewController.navigationItem.title = "INICdffsfIO"
        if typeTextField == "finishTravel"
        {
            
            if place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n") == ""
            {
                finishTravel.text =  place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n")
            }
            else
            {
                finishTravel.text = place.name
            }
            latitudeFinish = place.coordinate.latitude
            longitudeFinish = place.coordinate.longitude
            addMarkerInMap(lat: place.coordinate.latitude, long: place.coordinate.longitude, title: "Destino", snippet: "")
            let loc : CLLocation = CLLocation(latitude: latitudeFinish, longitude: longitudeFinish)
            updateMapFrame(newLocation: loc, zoom: 15.0)
        }
        else
        {
            if place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n") == ""
            {
                initTravel.text =  place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n")
            }
            else
            {
                initTravel.text =   place.name
            }
            latitudeInit = place.coordinate.latitude
            longitudeInit = place.coordinate.longitude
            addMarkerInMap(lat: latitudeInit , long: longitudeInit , title: "Inicio", snippet: "")
            let loc : CLLocation = CLLocation(latitude: latitudeInit, longitude: longitudeInit)
            updateMapFrame(newLocation: loc, zoom: 15.0)
        }
        
        if ((initTravel.text != "") && (finishTravel.text != ""))
        {
            labelPriceTravel.text = "Calculando datos de viaje"
            DispatchQueue.main.async {
                self.calculatePriceRoute(latitudeInit: self.latitudeInit, longitudeInit: self.longitudeInit, latitudeFinish: self.latitudeFinish, longitudeFinish: self.longitudeFinish)
            }
        }
        
        
        viewController.dismiss(animated: true, completion: nil)
        
        //self.lblName.text = place.name
        //self.lblAddress.text = place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n")
        //self.lblLatitude.text = String(place.coordinate.latitude)
        //self.lblLongitude.text = String(place.coordinate.longitude)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        
        viewController.dismiss(animated: true, completion: nil)
        
        //self.viewContainer.isHidden = true
       // self.indicatorView.isHidden = true
    }
    
    func drawRoute(latitudeInit : Double , longitudeInit: Double, latitudeFinish : Double, longitudeFinish : Double)
    {
        DispatchQueue.main.async {
            let origin = "\(latitudeInit),\(longitudeInit)"
            let destination = "\(latitudeFinish),\(longitudeFinish)"
            
            let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(llaveMapa)"
            print(urlString)
        
            
            let url = URL(string: urlString)
            URLSession.shared.dataTask(with: url!, completionHandler: {
                (data, response, error) in
                if(error != nil){
                    print("error")
                }else{
                    do{
                        let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                        let routes = json["routes"] as! NSArray
                        //self.mapView.clear()
                        
                        
                        OperationQueue.main.addOperation({
                            for route in routes
                            {
                                let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                                let points = routeOverviewPolyline.object(forKey: "points")
                                let path = GMSPath.init(fromEncodedPath: points! as! String)
                                let polyline = GMSPolyline.init(path: path)
                                polyline.strokeWidth = 3
                                polyline.strokeColor = UIColor.black
                                
                                let bounds = GMSCoordinateBounds(path: path!)
                                self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                                
                                polyline.map = self.mapView
                                
                            }
                        })
                    }catch let error as NSError{
                        print("error:\(error)")
                    }
                }
            }).resume()
        }
    }
    
    func calculatePriceRoute(latitudeInit : Double , longitudeInit: Double, latitudeFinish : Double, longitudeFinish : Double)
    {
        DispatchQueue.main.async {
            
            self.mapView.clear()
            
            
            let origin = "\(latitudeInit),\(longitudeInit)"
            let destination = "\(latitudeFinish),\(longitudeFinish)"
            
            self.addMarkerInMap(lat: latitudeInit, long: longitudeInit, title: "Inicio", snippet: "")
            self.addMarkerInMap(lat: latitudeFinish, long: longitudeFinish, title: "Destino", snippet: "")
            
            let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(llaveMapa)"
            print(urlString)
            

            
            let url = URL(string: urlString)
            URLSession.shared.dataTask(with: url!, completionHandler: {
                (data, response, error) in
                if(error != nil){
                    print("error")
                }else{
                    do{
                        let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                        
                        let jsonTrack = JSON(json)
                        let routes = jsonTrack["routes"]
                        let routes2 = json["routes"] as! NSArray
                        print ("ROUTES")
                        print(routes)
                        
                        print (routes.count)
                        if routes.count != 0
                        {
                            OperationQueue.main.addOperation({
                                for route in routes2
                                {
                                    let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                                    let points = routeOverviewPolyline.object(forKey: "points")
                                    let path = GMSPath.init(fromEncodedPath: points! as! String)
                                    let polyline = GMSPolyline.init(path: path)
                                    polyline.strokeWidth = 3
                                    polyline.strokeColor = UIColor.black
                                    
                                    let bounds = GMSCoordinateBounds(path: path!)
                                    self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                                    
                                    polyline.map = self.mapView
                                    
                                }
                            })
                            
                            
                            let legs =  routes[0]["legs"]
                            print ("LEGS")
                            print(legs)
                            
                            let duration =  legs[0]["duration"]
                            print ("duration")
                            print(duration)
                            
                            let distance =  legs[0]["distance"]
                            print ("distance")
                            print(distance)
                            
                            let minutes = String(describing: duration["value"])
                            let meters = String(describing: distance["value"])
                            
                            print(minutes)
                            print(meters)
                            
                            self.minutesTravel = String(Double(minutes)! / 60.0)
                            self.kilometersTravel = String(Double(meters)! / 1000.0)
                            
                            DispatchQueue.main.async {
                                self.calculatePriceTravel();
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                
                                self.labelPriceTravel.text = "No se ha podido calcular una tarifa"
                            }
                            
                        }
                        
                        
                        
                        
                    }catch let error as NSError{
                        print("error:\(error)")
                        self.labelPriceTravel.text = "No se ha podido calcular una tarifa"
                        //self.calculatePriceRoute(latitudeInit: self.latitudeInit,  longitudeInit:  self.longitudeInit, latitudeFinish:  self.latitudeFinish, longitudeFinish:  self.longitudeFinish)
                    }
                }
            }).resume()
        }
    }
    
    func pageSetUp()  {
        
        //Pass view controller delegate on view model page
        objMapModel.delegate = self
        //mapview delegate settings and inial location set
        mapView.delegate = self
        //mapView.camera = camera
        
        objMapModel.jsonDataRead()
        
    }
}

// TRACK ROUTE
extension MapViewController{
    
    //Success json read delegate method
    func isSucessReadJson()  {
        drawPathOnMap()
    }
    
    //fail json read delegate method
    func isFailReadJson(msg : String)  {
        let alert = UIAlertController(title: "Map Alert", message: msg, preferredStyle: .alert)
        let actionOK : UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(actionOK)
        self.present(alert, animated: true, completion: nil)
    }
}

extension MapViewController{
    
    //path create
    func drawPathOnMap()  {
        let path = GMSMutablePath()
        let marker = GMSMarker()
        
        let inialLat:Double = objMapModel.arrayMapPath[0].lat!
        let inialLong:Double = objMapModel.arrayMapPath[0].lon!
        
        for mapPath in objMapModel.arrayMapPath
        {
            path.add(CLLocationCoordinate2DMake(mapPath.lat!, mapPath.lon!))
        }
        //set poly line on mapview
        let rectangle = GMSPolyline(path: path)
        rectangle.strokeWidth = 5.0
        marker.map = mapView
        rectangle.map = mapView
        
        //Zoom map with path area
        let loc : CLLocation = CLLocation(latitude: inialLat, longitude: inialLong)
        updateMapFrame(newLocation: loc, zoom: 20.0)
    }
    
 
    
    //marker move on map view
    func updateDriver()
    {
        let loc : CLLocation = CLLocation(latitude: latitudeDriver, longitude: longitudeDriver)
        //self.updateMapFrame(newLocation: loc, zoom: self.mapView.camera.zoom)
        
        marker.position = CLLocationCoordinate2DMake(latitudeDriver, longitudeDriver)
        marker.rotation = angleDriver
        
        marker.icon = UIImage(named: "map_car_running.png")
        marker.map = mapView
        
        // Timer close
        if flagFinishTimer == false
        {
            // timer close
            timer.invalidate()
            showFinishTravel()
            //buttonPlay.isEnabled = true
            iTemp = 0
        }
        iTemp += 1
    }
    
    
    //Zoom map with cordinate
    func updateMapFrame(newLocation: CLLocation, zoom: Float) {
        
       
            UIView.animate(withDuration: 2.0, animations: {
                let camera = GMSCameraPosition.camera(withTarget: newLocation.coordinate, zoom: zoom)
                self.mapView.animate(to: camera)
                //self.mapView.reloadInputViews()
                
                //self.mapView.hitTest(CGPoint(x: 400, y: 400), with: nil)
                //self.mapView.hitTest(CGPoint(x: 400, y: 400), with: nil)
                
                //let point = CGPoint(x: 0.0, y: (textView.contentSize.height - textView.bounds.height))
                //self.mapView.scroll
            })
        
        

        //self.mapView.hitTest(CGPoint(x: 400, y: 400), with: nil)
        
    }
    
    func showFinishTravel()
    {
        
    }
    
    public func cancelTravel()
    {
        DispatchQueue.main.async {
            let status = String(describing: UserDefaults.standard.value(forKey: DataPersistent.status_viaje)!)
            if ((status == "0") || (status == "1"))
            {
                
                
                
                let alert = UIAlertController(title: "Taxsi", message: "¿Desea cancelar su viaje?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Continuar Viaje", style: UIAlertActionStyle.default, handler: { action in
                    
                    self.alertPleaseWait()
                }))
                
                alert.addAction(UIAlertAction(title: "Cancelar Viaje", style: UIAlertActionStyle.destructive, handler: { action in
                    
                    self.requestCancelTravel();
                    
                    
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
                
            else
            {
                let alert = UIAlertController(title: "Taxsi", message: "Ya no es posible cancelar un viaje en progreso", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { action in
                }))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    
    func requestTrip2(id_pasajero_var : String,id_viaje_var : String)
    {
        DispatchQueue.main.async {
            HUD.show(.progress)
            
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            
            let parameters: Parameters = [
                "id_viaje" : id_viaje_var,
                "id_pasajero": id_pasajero_var
            ]
            
            print("WS_REQUEST_TRAVEL_2")
            print("\(ApiDefinition.WS_REQUEST_TRAVEL_2)?id_viaje=\(id_viaje_var)&id_pasajero=\(id_pasajero_var)")
            Alamofire.request(ApiDefinition.WS_REQUEST_TRAVEL_2, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("JSON_WS_REQUEST_TRAVEL_2: \(json)")
                    let resultado = json["resultado"]
                    let res = resultado[0]["resultado"]
                    if res != 0
                    {
                        
                        //let id_viaje = resultado[0]["id_viaje"].string
                        //print(id_viaje!)
                        //UserDefaults.standard.setValue(id_viaje, forKey: DataPersistent.id_viaje)
                        //UserDefaults.standard.setValue("1", forKey: DataPersistent.status_viaje)
                        //UserDefaults.standard.setValue(self.initTravel.text!, forKey: DataPersistent.initTravel)
                        //UserDefaults.standard.setValue(self.finishTravel.text!, forKey: DataPersistent.finishTravel)
                        //UserDefaults.standard.setValue(self.latitudeInit, forKey: DataPersistent.initTravelLatitude)
                        //UserDefaults.standard.setValue(self.longitudeInit, forKey: DataPersistent.initTravelLongitude)
                        //UserDefaults.standard.setValue(self.latitudeFinish, forKey: DataPersistent.finishTravelLatitude)
                        //UserDefaults.standard.setValue(self.longitudeFinish, forKey: DataPersistent.finishTravelLongitude)
                        //UserDefaults.standard.synchronize()
                        //HUD.flash(.success, delay: 0.5)
                        
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            
                            let message = "Por favor espere un momento..."
                            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                            //self.present(alert, animated: true)
                            
                            // duration in seconds
                            let duration: Double = 2
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                                alert.dismiss(animated: true)
                            }
                            
                            
                            self.alertPleaseWait()
                            
                            //self.callTaxi.setImage(UIImage(), for: UIControlState.normal)
                            //self.callTaxi.setTitle("CANCELAR VIAJE", for: UIControlState.normal)
                            //self.callTaxi.backgroundColor = UIColor.red
                            
                            self.timerOn()
                            
                        }
                        
                        
                    }
                    else
                    {
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            self.alertTravelNotCompleted()
                        }
                    }
                case .failure(let error):
                    print(error)
                    
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        self.alertErrorServer()
                    }
                    
                }
            }
        }
    }
    
    func requestTrip()
    {
        DispatchQueue.main.async {
            HUD.show(.progress)
            
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            
            let parameters: Parameters = [
                "id_pasajero": id,
                "lugar_partida": self.initTravel.text! as Any,
                "lugar_llegada": self.finishTravel.text! as Any,
                "latitud_partida": self.latitudeInit,
                "longitud_partida": self.longitudeInit,
                "latitud_llegada_sol": self.latitudeFinish,
                "longitud_llegada_sol": self.longitudeFinish
            ]
            
            print("WS_REQUEST_TRAVEL")
            print("\(ApiDefinition.WS_REQUEST_TRAVEL)?id_pasajero=\(id)&lugar_partida=\(self.initTravel.text! as Any)&lugar_llegada=\(self.finishTravel.text! as Any)&latitud_partida=\(self.latitudeInit)&longitud_partida=\(self.longitudeInit)&latitud_llegada_sol=\(self.latitudeFinish)&longitud_llegada_sol=\(self.longitudeFinish)")
            var request: Alamofire.Request?
            request = Alamofire.request(ApiDefinition.WS_REQUEST_TRAVEL, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("WS_REQUEST_TRAVEL: \(json)")
                    let resultado = json["resultado"]
                    let res = resultado[0]["resultado"]
                    if res != 0
                    {
                        
                        let id_viaje = resultado[0]["id_viaje"].string
                        print(id_viaje!)
                        UserDefaults.standard.setValue(id_viaje, forKey: DataPersistent.id_viaje)
                        UserDefaults.standard.setValue("1", forKey: DataPersistent.status_viaje)
                        UserDefaults.standard.setValue(self.initTravel.text!, forKey: DataPersistent.initTravel)
                        UserDefaults.standard.setValue(self.finishTravel.text!, forKey: DataPersistent.finishTravel)
                        UserDefaults.standard.setValue(self.latitudeInit, forKey: DataPersistent.initTravelLatitude)
                        UserDefaults.standard.setValue(self.longitudeInit, forKey: DataPersistent.initTravelLongitude)
                        UserDefaults.standard.setValue(self.latitudeFinish, forKey: DataPersistent.finishTravelLatitude)
                        UserDefaults.standard.setValue(self.longitudeFinish, forKey: DataPersistent.finishTravelLongitude)
                        UserDefaults.standard.synchronize()
                        //HUD.flash(.success, delay: 0.5)
                        
                        self.travelPreferentialtake = true
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            
                            let message = "Por favor espere un momento..."
                            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                            //self.present(alert, animated: true)
                            
                            // duration in seconds
                            let duration: Double = 2
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
                                alert.dismiss(animated: true)
                            }
                            
                            
                            self.alertPleaseWait()
                            
                            //self.callTaxi.setImage(UIImage(), for: UIControlState.normal)
                            //self.callTaxi.setTitle("CANCELAR VIAJE", for: UIControlState.normal)
                            //self.callTaxi.backgroundColor = UIColor.red
                            
                            self.timerOn()
                            
                        }
                        
                        
                    }
                    else
                    {
                        self.travelPreferentialtake = false
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            self.alertTravelNotCompleted()
                        }
                    }
                case .failure(let error):
                    print(error)
                    
                    self.travelPreferentialtake = false
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        self.alertErrorServer()
                    }
                }
            }
        }
        
    }
    
    //Si el usuario tiene un vieje de calificar pendiente, necesita que regrese el id viaje
    func pendingRatingTrip(arg: Bool, completion: @escaping (Bool) -> ())
    {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            
            let parameters: Parameters = [
                "id_pasajero": id
            ]
            
            print("WS_PENDING_EVALUATE_DRIVER")
            print("\(ApiDefinition.WS_PENDING_EVALUATE_DRIVER)?id_pasajero=\(id)")
            //print(parameters)
            Alamofire.request(ApiDefinition.WS_PENDING_EVALUATE_DRIVER, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("JSON_WS_PENDING_EVALUATE_DRIVER: \(json)")
                    let resultado = json["resultado"]
                    let res = resultado[0]["resultado"]
                    if res != 0
                    {
                        
                        let viaje_pendiente_calif = json["viaje_pendiente_calif"]
                        let id = String(describing: viaje_pendiente_calif[0]["id"])
                        
                        print ("id viaje: \(id)")
                        UserDefaults.standard.setValue(id, forKey: DataPersistent.id_viaje_pendiente)
                        UserDefaults.standard.synchronize()
                        
                        //                    let place = ObjectPendingTravel(id: id, name: "<#T##String#>", address: "<#T##String#>", city: "<#T##String#>", latitude: 1.423, longitude: 3.423, image: "<#T##String#>", distance: "<#T##String#>")
                        //
                        //                    ObjectPendingTravel.getTravel().add(place)
                        
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        completion(true)
                        
                    }
                    else
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        completion(false)
                    }
                case .failure(let error):
                    print(error)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    completion(false)
                }
            }
        }
    }
    
    func calculatePriceTravel() //Status del viaje, 0 libre, 1 viaje solicitado, 2 viaje en progreso
    {
        DispatchQueue.main.async {
            
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let parameters: Parameters = [
                "distancia": self.kilometersTravel,
                "tiempo": self.minutesTravel
            ]
            
            print("WS_CALCULATE_PRICE")
            print("\(ApiDefinition.WS_CALCULATE_PRICE)?distancia=\(self.kilometersTravel)&tiempo=\(self.minutesTravel)")
            
            Alamofire.request(ApiDefinition.WS_CALCULATE_PRICE, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("JSON_WS_CALCULATE_PRICE: \(json)")
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    let resultado = json["tarifa"]
                    let res = String(describing: resultado[0]["tarifa"])
                    
                    self.priceAprox = res
                    self.labelPriceTravel.text = "El costo aproximado del viaje es $\(res) pesos"
                    
                case .failure(let error):
                    print(error)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    DispatchQueue.main.async {
                        self.calculatePriceTravel();
                    }
                }
            }
        }
    }
    
    func statusTrip() //Status del viaje, 0 libre, 1 viaje solicitado, 2 viaje en progreso
    {
        DispatchQueue.main.async {
            
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let parameters: Parameters = [
                "id_pasajero": id
            ]
            
            print("WS_STATUS_TRAVEL")
            print("\(ApiDefinition.WS_STATUS_TRAVEL)?id_pasajero=\(id)")
            
            Alamofire.request(ApiDefinition.WS_STATUS_TRAVEL, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("JSON_WS_STATUS_TRAVEL: \(json)")
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    let resultado = json["estatus"]
                    let res = String(describing: resultado[0]["estatus"])
                    let id_viaje = String(describing: resultado[0]["id_viaje"])
                    print(res)
                    print(id_viaje)
                    self.flagUpdateInitTravel = true
                    self.actionStatusTravel(status: res, id_viaje: id_viaje)
                case .failure(let error):
                    print(error)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    loadingAlertController.dismiss(animated: true, completion: nil)
                    self.statusTrip();
                }
            }
        }
    }
    
    func actionStatusTravel(status : String , id_viaje : String)
    {
        
        let initTravelString = String(describing: UserDefaults.standard.value(forKey: DataPersistent.initTravel)!)
        let finishTravelString = String(describing: UserDefaults.standard.value(forKey: DataPersistent.finishTravel)!)
        print("initTravelString: \(initTravelString)" )
        print("finishTravelString: \(finishTravelString)" )
        
        //self.mapView.clear()
        
        if (status != "0")
        {
            let initTravelLatitude = String(describing: UserDefaults.standard.value(forKey: DataPersistent.initTravelLatitude)!)
            let initTravelLongitude = String(describing: UserDefaults.standard.value(forKey: DataPersistent.initTravelLongitude)!)
            let finishTravelLatitude = String(describing: UserDefaults.standard.value(forKey: DataPersistent.finishTravelLatitude)!)
            let finishTravelLongitude = String(describing: UserDefaults.standard.value(forKey: DataPersistent.finishTravelLongitude)!)
            
            print("COORDENADAS_GUARDADAS actionStatusTravel" )
            print("initTravelLatitude: \(initTravelLatitude)" )
            print("initTravelLongitude: \(initTravelLongitude)" )
            print("finishTravelLatitude: \(finishTravelLatitude)" )
            print("finishTravelLongitude: \(finishTravelLongitude)" )
            
            addMarkerInMap(lat: (initTravelLatitude as NSString).doubleValue, long: (initTravelLongitude as NSString).doubleValue, title: "Inicio", snippet: "")
            addMarkerInMap(lat: (finishTravelLatitude as NSString).doubleValue, long: (finishTravelLongitude as NSString).doubleValue, title: "Destino", snippet: "")
        }
        
        switch status {
        case "0":
//            let status_viaje = String(describing: status)
//            print(id_viaje)
//            print(status_viaje)
//            UserDefaults.standard.setValue(id_viaje, forKey: DataPersistent.id_viaje)
//            UserDefaults.standard.setValue(status_viaje, forKey: DataPersistent.status_viaje)
//            UserDefaults.standard.synchronize()
//            UIApplication.shared.isNetworkActivityIndicatorVisible = false
//
//            self.initTravel.text = initTravelString
//            self.finishTravel.text = finishTravelString
            
            resetTravel()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
        case "1": // Conductor en progreso, se debe de poder cancelar viaje
           
            let status_viaje = String(describing: status)
            print(id_viaje)
            print(status_viaje)
            UserDefaults.standard.setValue(id_viaje, forKey: DataPersistent.id_viaje)
            UserDefaults.standard.setValue(status_viaje, forKey: DataPersistent.status_viaje)
            UserDefaults.standard.synchronize()
            
            self.callTaxi.isHidden = true
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            self.timerOn()
            loadingAlertController.dismiss(animated: true, completion: nil)
            
            self.initTravel.text = initTravelString
            self.finishTravel.text = finishTravelString

            self.getDataTravel()
            //drawRoute(latitudeInit: latitudeInit, longitudeInit: longitudeInit, latitudeFinish: latitudeDriver, longitudeFinish: longitudeDriver)
            
            
        case "2":
            let status_viaje = String(describing: status)
            print(id_viaje)
            print(status_viaje)
            UserDefaults.standard.setValue(id_viaje, forKey: DataPersistent.id_viaje)
            UserDefaults.standard.setValue(status_viaje, forKey: DataPersistent.status_viaje)
            UserDefaults.standard.synchronize()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            self.timerOn()
            self.viewProfileDriver.isHidden = false
            self.callTaxi.isHidden = true
            loadingAlertController.dismiss(animated: true, completion: nil)
            
            self.initTravel.text = initTravelString
            self.finishTravel.text = finishTravelString
            self.getDataTravel()
            //drawRoute(latitudeInit: latitudeInit, longitudeInit: longitudeInit, latitudeFinish: latitudeFinish, longitudeFinish: longitudeFinish)
            
        case "3":
            let status_viaje = String(describing: status)
            print(id_viaje)
            print(status_viaje)
            UserDefaults.standard.setValue(id_viaje, forKey: DataPersistent.id_viaje)
            UserDefaults.standard.setValue(status_viaje, forKey: DataPersistent.status_viaje)
            UserDefaults.standard.synchronize()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            timer.invalidate()
            self.viewProfileDriver.isHidden = false
            self.callTaxi.isHidden = true
            loadingAlertController.dismiss(animated: true, completion: nil)
            
            resetTravel()
            
        case "4":
            
            let status_viaje = String(describing: status)
            print(id_viaje)
            print(status_viaje)
            UserDefaults.standard.setValue(id_viaje, forKey: DataPersistent.id_viaje)
            UserDefaults.standard.setValue(status_viaje, forKey: DataPersistent.status_viaje)
            UserDefaults.standard.synchronize()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            timer.invalidate()
            self.viewProfileDriver.isHidden = false
            self.callTaxi.isHidden = true
            loadingAlertController.dismiss(animated: true, completion: nil)
            
            resetTravel()
            

            
        default:
            print("Error status_viaje")
            loadingAlertController.dismiss(animated: true, completion: nil)
        }
    }
    
    func actionStatusTravelView(status : String , id_viaje : String)
    {
        let initTravelString = String(describing: UserDefaults.standard.value(forKey: DataPersistent.initTravel)!)
        let finishTravelString = String(describing: UserDefaults.standard.value(forKey: DataPersistent.finishTravel)!)
        
        switch status {
        case "0":
//            let status_viaje = String(describing: status)
//            print(id_viaje)
//            print(status_viaje)
//            UserDefaults.standard.setValue(id_viaje, forKey: DataPersistent.id_viaje)
//            UserDefaults.standard.setValue(status_viaje, forKey: DataPersistent.status_viaje)
//            UserDefaults.standard.synchronize()
//            self.initTravel.text = initTravelString
//            self.finishTravel.text = finishTravelString
            resetTravel()
            mapView.isMyLocationEnabled = true
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
        case "1": // Conductor en progreso, se debe de poder cancelar viaje
            
            let status_viaje = String(describing: status)
            print(id_viaje)
            print(status_viaje)
            UserDefaults.standard.setValue(id_viaje, forKey: DataPersistent.id_viaje)
            UserDefaults.standard.setValue(status_viaje, forKey: DataPersistent.status_viaje)
            UserDefaults.standard.synchronize()

            self.callTaxi.isHidden = true
            self.viewProfileDriver.isHidden = false
            
            self.initTravel.text = initTravelString
            self.finishTravel.text = finishTravelString
            self.initTravel.isEnabled = false
            self.finishTravel.isEnabled = false
            mapView.isMyLocationEnabled = true
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            loadingAlertController.dismiss(animated: true, completion: nil)
        
            
        case "2":
            let status_viaje = String(describing: status)
            print(id_viaje)
            print(status_viaje)
            UserDefaults.standard.setValue(id_viaje, forKey: DataPersistent.id_viaje)
            UserDefaults.standard.setValue(status_viaje, forKey: DataPersistent.status_viaje)
            UserDefaults.standard.synchronize()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
            self.viewProfileDriver.isHidden = false
            self.callTaxi.isHidden = true
            loadingAlertController.dismiss(animated: true, completion: nil)
            
            self.initTravel.text = initTravelString
            self.finishTravel.text = finishTravelString
            self.initTravel.isEnabled = false
            self.finishTravel.isEnabled = false
            mapView.isMyLocationEnabled = false
            
        case "3":
            let status_viaje = String(describing: status)
            print(id_viaje)
            print(status_viaje)
            UserDefaults.standard.setValue(id_viaje, forKey: DataPersistent.id_viaje)
            UserDefaults.standard.setValue(status_viaje, forKey: DataPersistent.status_viaje)
            UserDefaults.standard.synchronize()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            self.viewProfileDriver.isHidden = true
            self.callTaxi.isHidden = false
            loadingAlertController.dismiss(animated: true, completion: nil)
            
            timer.invalidate()
            self.initTravel.text = ""
            self.finishTravel.text = ""
            self.initTravel.isEnabled = true
            self.finishTravel.isEnabled = true
            mapView.isMyLocationEnabled = true
            
            self.showDialog(.fadeInOut)
            
            resetTravel()
            
            
            
        case "4":
            

            
            let status_viaje = String(describing: status)
            print(id_viaje)
            print(status_viaje)
            UserDefaults.standard.setValue(id_viaje, forKey: DataPersistent.id_viaje)
            UserDefaults.standard.setValue(status_viaje, forKey: DataPersistent.status_viaje)
            UserDefaults.standard.synchronize()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            self.viewProfileDriver.isHidden = true
            self.callTaxi.isHidden = false
            loadingAlertController.dismiss(animated: true, completion: nil)
            
            timer.invalidate()
            self.initTravel.text = ""
            self.finishTravel.text = ""
            self.initTravel.isEnabled = true
            self.finishTravel.isEnabled = true
            mapView.isMyLocationEnabled = true
    
            resetTravel()
            
            if flagAutoCancel == false
            {
                let alert = UIAlertController(title: "Taxsi", message: "Se ha cancelado su viaje", preferredStyle: .alert)
                let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                alert.addAction(actionOK)
                self.present(alert, animated: true, completion: nil)
            }
            
        default:
            print("Error status_viaje")
            loadingAlertController.dismiss(animated: true, completion: nil)
        }
    }
    
    func getDataTravel()
    {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            let id_viaje = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
            
            let parameters: Parameters = [
                "id_pasajero": id,
                "id_viaje" : id_viaje
            ]
            
            print("WS_DATA_TRAVEL")
            print("\(ApiDefinition.WS_DATA_TRAVEL)?id_pasajero=\(id)&id_viaje=\(id_viaje)")
            
            Alamofire.request(ApiDefinition.WS_DATA_TRAVEL, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("JSON_WS_DATA_TRAVEL: \(json)")
                    let resultado = json["resultado"]
                    print("resultado: \(resultado)")
                    let res = resultado[0]["resultado"]
                    if res == 1
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let viaje = json["viaje"]
                        
                        let fecha_solicitud = String(describing: viaje[0]["fecha_solicitud"])
                        let estatus = String(describing:viaje[0]["estatus"])
                        let latitud_partida = String(describing:viaje[0]["latitud_partida"])
                        let latitud_llegada_sol = String(describing:viaje[0]["latitud_llegada_sol"])
                        let longitud_llegada_real = String(describing:viaje[0]["longitud_llegada_real"])
                        let nombre_conductor = String(describing:viaje[0]["nombre_conductor"])
                        let fecha_llegada = String(describing:viaje[0]["fecha_llegada"])
                        let longitud_partida = String(describing:viaje[0]["longitud_partida"])
                        let id_conductor = String(describing:viaje[0]["id_conductor"])
                        let id = String(describing:viaje[0]["id"])
                        let fecha_inicio = String(describing:viaje[0]["fecha_inicio"])
                        let tel_conductor = String(describing:viaje[0]["tel_conductor"])
                        let nombre_pasajero = String(describing:viaje[0]["nombre_pasajero"])
                        let foto_conductor = String(describing:viaje[0]["foto_conductor"])
                        let distance = String(describing:viaje[0]["distance"])
                        let id_pasajero = String(describing:viaje[0]["id_pasajero"])
                        let placas = String(describing:viaje[0]["placas"])
                        let tel_pasajero = String(describing:viaje[0]["tel_pasajero"])
                        let lugar_llegada = String(describing:viaje[0]["lugar_llegada"])
                        let unidad = String(describing:viaje[0]["unidad"])
                        let longitud_llegada_sol = String(describing:viaje[0]["longitud_llegada_sol"])
                        let longitud_conductor = String(describing:viaje[0]["longitud_conductor"])
                        let lugar_partida = String(describing:viaje[0]["lugar_partida"])
                        let modelo = String(describing:viaje[0]["modelo"])
                        let latitud_conductor = String(describing:viaje[0]["latitud_conductor"])
                        let promedio_conductor = String(describing:viaje[0]["promedio_conductor"])
                        
                        
                        let data : ObjectDataTravel = ObjectDataTravel(fecha_solicitud: fecha_solicitud, estatus: estatus, latitud_partida: latitud_partida, latitud_llegada_sol: latitud_llegada_sol, longitud_llegada_real: longitud_llegada_real, nombre_conductor: nombre_conductor, fecha_llegada: fecha_llegada, distance: distance, longitud_partida: longitud_partida, id_conductor: id_conductor, id: id, fecha_inicio: fecha_inicio, tel_conductor: tel_conductor, nombre_pasajero: nombre_pasajero, foto_conductor: foto_conductor, id_pasajero: id_pasajero, correo_conductor: nombre_conductor, placas: placas, latitud_llegada_real: latitud_llegada_sol, tel_pasajero: tel_pasajero, lugar_llegada: lugar_llegada, unidad: unidad, longitud_llegada_sol: longitud_llegada_sol, correo_pasajero: nombre_pasajero, longitud_conductor: longitud_conductor, lugar_partida: lugar_partida, modelo: modelo, latitud_conductor: latitud_conductor, promedio_conductor : promedio_conductor)
                        
                        ObjectDataTravel.getData().add(data)
                        
                        nameDriverGlobal.text = nombre_conductor
                        carGlobal.text =  unidad
                        licensePlateGlobal.text =  placas
                        starsGlobal.text = promedio_conductor
                        
                        UserDefaults.standard.setValue(lugar_partida, forKey: DataPersistent.initTravel)
                        UserDefaults.standard.setValue(lugar_llegada, forKey: DataPersistent.finishTravel)
                        UserDefaults.standard.setValue(latitud_partida, forKey: DataPersistent.initTravelLatitude)
                        UserDefaults.standard.setValue(longitud_partida, forKey: DataPersistent.initTravelLongitude)
                        UserDefaults.standard.setValue(latitud_llegada_sol, forKey: DataPersistent.finishTravelLatitude)
                        UserDefaults.standard.setValue(longitud_llegada_sol, forKey: DataPersistent.finishTravelLongitude)
                        
                        
                        UserDefaults.standard.synchronize()
                        
                        self.initTravel.text = lugar_partida
                        self.finishTravel.text = lugar_llegada
                        
                        //                    let initTravelLatitude = String(describing: UserDefaults.standard.value(forKey: DataPersistent.initTravelLatitude)!)
                        //                    let initTravelLongitude = String(describing: UserDefaults.standard.value(forKey: DataPersistent.finishTravelLongitude)!)
                        //                    let finishTravelLatitude = String(describing: UserDefaults.standard.value(forKey: DataPersistent.finishTravelLatitude)!)
                        //                    let finishTravelLongitude = String(describing: UserDefaults.standard.value(forKey: DataPersistent.finishTravelLongitude)!)
                        
                        print("TEST1" )
                        print("initTravelLatitude: \(latitud_partida)" )
                        print("initTravelLongitude: \(longitud_partida)" )
                        print("finishTravelLatitude: \(latitud_llegada_sol)" )
                        print("finishTravelLongitude: \(longitud_llegada_sol)" )
                        
                        if (self.priceAprox == "0.0" )
                        {
                            if ((estatus != "3") && (estatus != "4"))
                            {
                                print("Entro a calcular ruta")
                                DispatchQueue.main.async {
                                    self.calculatePriceRoute(latitudeInit: Double(latitud_partida)!, longitudeInit: Double(longitud_partida)!, latitudeFinish: Double(latitud_llegada_sol)!, longitudeFinish: Double(longitud_llegada_sol)!)
                                }
                            }
                            else
                            {
                                self.resetTravel()
                            }
                        }
                    }
                    else
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                case .failure(let error):
                    print(error)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.getDataTravel()
                }
            }
        }
    }
    
    func checkTravelIncomplete()
    {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            
            let parameters: Parameters = [
                "id_pasajero": id,
            ]
            
            print("WS_CANCEL_TRAVELS")
            print("\(ApiDefinition.WS_CANCEL_TRAVELS)?id_pasajero=\(id)")
            
            Alamofire.request(ApiDefinition.WS_CANCEL_TRAVELS, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    
                    self.statusTrip()
                    self.getDataTravel()
                    
                    let json = JSON(value)
                    print("JSON_WS_CANCEL_TRAVELS: \(json)")
                    let resultado = json["resultado"]
                    print("resultado Cancel: \(resultado)")
                    let res = resultado[0]["resultado"]
                    if res == 1
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        
                        self.resetTravel()
                        
                        //UserDefaults.standard.setValue(lugar_partida, forKey: DataPersistent.initTravel)
                        //                    UserDefaults.standard.setValue(lugar_llegada, forKey: DataPersistent.finishTravel)
                        //                    UserDefaults.standard.setValue(latitud_partida, forKey: DataPersistent.initTravelLatitude)
                        //                    UserDefaults.standard.setValue(longitud_partida, forKey: DataPersistent.initTravelLongitude)
                        //                    UserDefaults.standard.setValue(latitud_llegada_sol, forKey: DataPersistent.finishTravelLatitude)
                        //                    UserDefaults.standard.setValue(longitud_llegada_sol, forKey: DataPersistent.finishTravelLongitude)
                        
                        
                        UserDefaults.standard.synchronize()
                        
                        self.initTravel.text = ""
                        self.finishTravel.text = ""
                        
                    }
                    else
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                case .failure(let error):
                    print(error)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.checkTravelIncomplete()
                }
            }
        }
    }
    
    func requestCancelTravel()
    {
        DispatchQueue.main.async {
            HUD.show(.progress)
            self.flagAutoCancel = true
            
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            let id_viaje = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
            
            let parameters: Parameters = [
                "id_pasajero": id,
                "id_viaje" : id_viaje
            ]
            
            print("WS_CANCEL_TRAVEL")
            print("\(ApiDefinition.WS_CANCEL_TRAVEL)?id_pasajero=\(id)&id_viaje=\(id_viaje)")
            
            Alamofire.request(ApiDefinition.WS_CANCEL_TRAVEL, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("WS_CANCEL_TRAVEL: \(json)")
                    let resultado = json["resultado"]
                    let res = resultado[0]["resultado"]
                    if res != 0
                    {
                        timer.invalidate()
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            
                            UserDefaults.standard.setValue("0", forKey: DataPersistent.id_viaje)
                            UserDefaults.standard.setValue("0", forKey: DataPersistent.status_viaje)
                            UserDefaults.standard.setValue("", forKey: DataPersistent.initTravel)
                            UserDefaults.standard.setValue("", forKey: DataPersistent.finishTravel)
                            UserDefaults.standard.synchronize()
                            self.callTaxi.setImage(UIImage(named: "llamar.png"), for: UIControlState.normal)
                            self.callTaxi.backgroundColor = UIColor.clear
                            
                            
                            let alert = UIAlertController(title: "Taxsi", message: "Viaje cancelado correctamente", preferredStyle: .alert)
                            let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                            alert.addAction(actionOK)
                            self.present(alert, animated: true, completion: nil)
                            self.mapView.clear()
                            self.viewProfileDriver.isHidden = true
                            self.callTaxi.isHidden = false
                            self.initTravel.text = ""
                            self.finishTravel.text = ""
                            
                            self.resetTravel()
                            
                        }
                    }
                    else
                    {
                        self.flagAutoCancel = false
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            let alert = UIAlertController(title: "Taxsi", message: "No se ha podido cancelar el viaje", preferredStyle: .alert)
                            let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                            alert.addAction(actionOK)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                case .failure(let error):
                    print(error)
                    self.flagAutoCancel = false
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        self.alertErrorServer()
                    }
                }
            }
        }
    }
    
    
    func getCoordDriver()
    {
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            
            let var_id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            let var_id_viaje = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
            
            let parameters: Parameters = [
                "id_viaje" : var_id_viaje
            ]
            
            //print("WS_GET_COOR_DRIVER")
            //print("\(ApiDefinition.WS_GET_COOR_DRIVER)?id_viaje=\(var_id_viaje)")
            
            Alamofire.request(ApiDefinition.WS_GET_COOR_DRIVER, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    //print("WS_GET_COOR_DRIVER: \(json)")
                    let resultado = json["resultado"]
                    let res = resultado[0]["resultado"]
                    if res == 1
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let viaje = json["viaje"]
                        let status = String(describing:viaje[0]["estatus"])
                        let id = String(describing:viaje[0]["id"])
                        let latitude : String = String(describing:viaje[0]["latitud"])
                        let longitude : String = String(describing:viaje[0]["longitud"])
                        let angulo : String = String(describing:viaje[0]["angulo"])
                        UserDefaults.standard.setValue(status, forKey: DataPersistent.status_viaje)
                        UserDefaults.standard.setValue(id, forKey: DataPersistent.id_viaje)
                        UserDefaults.standard.setValue(id, forKey: DataPersistent.id_viaje_pendiente)
                        UserDefaults.standard.synchronize()
                        self.longitudeDriver = (longitude as NSString).doubleValue
                        self.latitudeDriver = (latitude as NSString).doubleValue
                        
                        if angulo != ""
                        {
                            self.angleDriver = (angulo as NSString).doubleValue
                        }
                        
                        self.updateDriver()
                        if (self.flagUpdateInitTravel == false)
                        {
                            self.flagUpdateInitTravel = true
                            self.statusTrip()
                            //self.actionStatusTravel(status: status, id_viaje: id)
                        }
                        print("STATUS \(status) counterTimer \(self.counterTimer) ")
                        self.actionStatusTravelView(status: status, id_viaje: id)
                        self.intentTrackRoute(status: status)
                        
                        
                        
                    }
                    else
                    {
                        print("counterTimer "  + String(self.counterTimer))
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let estatus = String(describing:resultado[0]["estatus"])
                        let id = String(describing:resultado[0]["id"])
                        //let status = String(describing:viaje[0]["estatus"])
                        
                        if estatus == "4"
                        {
                            self.actionStatusTravelView(status: estatus, id_viaje: id)
                        }
                        
                        if ((estatus == "null")&&(id == "null"))
                        {
                            //timer.invalidate()
                        }
                        
                        if ((self.counterTimer == 10))
                        {
                            print("INVALIDATE TRAVEL")
                            print("requestTrip2")
                            //timer.invalidate()
                            self.requestTrip2(id_pasajero_var: var_id, id_viaje_var: var_id_viaje )
                        }
                    }
                case .failure(let error):
                    print(error)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
        }
    }
    

    func getFavorites()
    {
        DispatchQueue.main.async {
            HUD.show(.progress)
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            let parameters: Parameters = [
                "id_pasajero" : id
            ]
            print("WS_GET_FAVORITES")
            print("\(ApiDefinition.WS_GET_FAVORITES)?id_pasajero=\(id)")
            Alamofire.request(ApiDefinition.WS_GET_FAVORITES, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("WS_GET_FAVORITES: \(json)")
                    let resultado = json["resultado"]
                    let res = resultado[0]["resultado"]
                    if res != 0
                    {
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            
                            
                            ObjectFavorites.getData().removeAllObjects()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            let lugar = json["lugar"]
                            
                            for index in 0..<(Int(lugar.array!.count)) {
                                let latitud = String(describing:lugar[index]["latitud"])
                                let longitud = String(describing:lugar[index]["longitud"])
                                let id = String(describing:lugar[index]["id"])
                                let nombre_lugar = String(describing:lugar[index]["nombre_lugar"])
                                print(nombre_lugar)
                                
                                
                                let object =  ObjectFavorites(id: id, latitud: latitud, longitud: longitud, nombre_lugar: nombre_lugar)
                                
                                
                                ObjectFavorites.getData().add(object)
                                
                            }
                            
                            if (ObjectFavorites.getData().count != 0)
                            {
                                self.addFavoriteTextField()
                            }
                            else
                            {
                                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                                    let alert = UIAlertController(title: "Taxsi", message: "No tiene favoritos aún", preferredStyle: .alert)
                                    let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                                    alert.addAction(actionOK)
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                            
                            
                            //self.tableView.reloadData()
                            
                            
                        }
                    }
                    else
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            let alert = UIAlertController(title: "Taxsi", message: "No se han podido cargar sus favoritos", preferredStyle: .alert)
                            let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                            alert.addAction(actionOK)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                case .failure(let error):
                    print(error)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
        }
    }
    
    func addFavorite()
    {
        DispatchQueue.main.async {
            HUD.show(.progress)
            
            
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            let id_viaje = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
            
            let parameters: Parameters = [
                "id_pasajero": id,
                "nombre_lugar" : "id_viaje",
                "latitud" : "" ,
                "longitud" : "" ,
            ]
            
            print("WS_ADD_FAVORITE")
            print("\(ApiDefinition.WS_ADD_FAVORITE)?id_pasajero=\(id)&nombre_lugar=\(id_viaje)&latitud=\(id_viaje)&longitud=\(id_viaje)")
            
            Alamofire.request(ApiDefinition.WS_ADD_FAVORITE, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("WS_ADD_FAVORITE: \(json)")
                    let resultado = json["resultado"]
                    let res = resultado[0]["resultado"]
                    if res != 0
                    {
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            
                        }
                    }
                    else
                    {
                        
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            
                        }
                    }
                case .failure(let error):
                    print(error)
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        
                    }
                }
            }
        }
    }
    
    
    func eraseFavorite()
    {
        DispatchQueue.main.async {
            HUD.show(.progress)
            
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            
            let parameters: Parameters = [
                "id_pasajero": id,
                "id_lugar" : "1"
            ]
            
            print("WS_ERASE_FAVORITE")
            print("\(ApiDefinition.WS_ERASE_FAVORITE)?id_pasajero=\(id)&id_lugar=\("id_viaje")")
            
            Alamofire.request(ApiDefinition.WS_ERASE_FAVORITE, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("WS_ERASE_FAVORITE: \(json)")
                    let resultado = json["resultado"]
                    let res = resultado[0]["resultado"]
                    if res != 0
                    {
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            
                        }
                    }
                    else
                    {
                        
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            
                        }
                    }
                case .failure(let error):
                    print(error)
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        
                    }
                }
            }
        }
    }
    

    
    
    func intentTrackRoute(status : String)
    {
        
        let initTravelLatitude = String(describing: UserDefaults.standard.value(forKey: DataPersistent.initTravelLatitude)!)
        let initTravelLongitude = String(describing: UserDefaults.standard.value(forKey: DataPersistent.initTravelLongitude)!)
        let finishTravelLatitude = String(describing: UserDefaults.standard.value(forKey: DataPersistent.finishTravelLatitude)!)
        let finishTravelLongitude = String(describing: UserDefaults.standard.value(forKey: DataPersistent.finishTravelLongitude)!)
        
         print(status)
         print(self.flagStatus)
        if ((status != self.flagStatus) || (self.flagStatus == ""))
        {
            print(initTravelLatitude)
            
            if ((initTravelLatitude == "0.0") || (longitudeDriver == 0.0) )
            {
                self.flagStatus = ""
            }
            else
            {
                self.flagStatus = status
                if (self.flagStatus == "1")
                {
                    self.mapView.clear()

                    
                    self.addMarkerInMap(lat: (initTravelLatitude as NSString).doubleValue, long: (initTravelLongitude as NSString).doubleValue, title: "Inicio", snippet: "")
                    self.addMarkerInMap(lat: (finishTravelLatitude as NSString).doubleValue, long: (finishTravelLongitude as NSString).doubleValue, title: "Destino", snippet: "")
                    
                     self.drawRoute(latitudeInit: (initTravelLatitude as NSString).doubleValue, longitudeInit: (initTravelLongitude as NSString).doubleValue, latitudeFinish: latitudeDriver, longitudeFinish: longitudeDriver)
                    
                }
                else if (self.flagStatus == "2")
                {
                    self.mapView.clear()

                    
                    self.addMarkerInMap(lat: (initTravelLatitude as NSString).doubleValue, long: (initTravelLongitude as NSString).doubleValue, title: "Inicio", snippet: "")
                    self.addMarkerInMap(lat: (finishTravelLatitude as NSString).doubleValue, long: (finishTravelLongitude as NSString).doubleValue, title: "Destino", snippet: "")
                    
                    self.drawRoute(latitudeInit: (initTravelLatitude as NSString).doubleValue, longitudeInit: (initTravelLongitude as NSString).doubleValue, latitudeFinish: (finishTravelLatitude as NSString).doubleValue, longitudeFinish: (finishTravelLongitude as NSString).doubleValue)
                }
                else
                {
                    
                }
            }
        }
    }
    
    func alertTravelNotCompleted()
    {
        let alert = UIAlertController(title: "Taxsi", message: "No se ha encontrado un taxi disponible, por favor intente nuevamente", preferredStyle: .alert)
        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
        alert.addAction(actionOK)
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertErrorServer()
    {
        let alert = UIAlertController(title: "Taxsi", message: "Ha ocurrido un problema con el servidor, por favor intente nuevamente", preferredStyle: .alert)
        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
        alert.addAction(actionOK)
        self.present(alert, animated: true, completion: nil)
    }
    
    func alertPleaseWait()
    {
        DispatchQueue.main.async() {
            loadingAlertController.dismiss(animated: true, completion: nil)
       
        
        loadingAlertController = UIAlertController(title: "", message: "Por favor espere, no cierre su aplicación", preferredStyle: .alert)
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        loadingAlertController.view.addSubview(activityIndicator)
        
        loadingAlertController.addAction(UIAlertAction(title: "Cancelar", style: UIAlertActionStyle.destructive, handler: { action in
            self.cancelTravel();
        }))
        
        let xConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .centerX, relatedBy: .equal, toItem: loadingAlertController.view, attribute: .centerX, multiplier: 1, constant: 0)
    
        let yConstraint = NSLayoutConstraint(item: activityIndicator, attribute: .centerY, relatedBy: .equal, toItem: loadingAlertController.view, attribute: .centerY, multiplier: 1.0, constant: 0)
        
        NSLayoutConstraint.activate([ xConstraint, yConstraint])
        activityIndicator.isUserInteractionEnabled = false
        activityIndicator.startAnimating()
        
        let height: NSLayoutConstraint = NSLayoutConstraint(item: loadingAlertController.view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: 150)
        
        loadingAlertController.view.addConstraint(height);
        self.present(loadingAlertController, animated: true, completion: nil)
             }
    }
    
    public func resetTravel()
    {
//        do {
//            let result =  try mapView.clear()
//        } catch {
//             print("error clear map")
//        }
        
      
        timer.invalidate()
        timer = Timer()
        if mapView != nil
        {
            mapView.clear() // if an error was thrown, CRASH!
        }
        
    
        counterTimer = 0
        flagFinishTimer = true
        flagUpdateInitTravel = false
        flagAutoCancel = false
        travelPreferentialtake = false
        
        UserDefaults.standard.setValue("", forKey: DataPersistent.id_viaje)
        UserDefaults.standard.setValue("0", forKey: DataPersistent.status_viaje)
        UserDefaults.standard.setValue("", forKey: DataPersistent.initTravel)
        UserDefaults.standard.setValue("", forKey: DataPersistent.finishTravel)
        UserDefaults.standard.setValue("", forKey: DataPersistent.initTravelLatitude)
        UserDefaults.standard.setValue("", forKey: DataPersistent.initTravelLongitude)
        UserDefaults.standard.setValue("", forKey: DataPersistent.finishTravelLatitude)
        UserDefaults.standard.setValue("", forKey: DataPersistent.finishTravelLongitude)
        
        ObjectDataTravel.getData().removeAllObjects()
        ObjectPendingTravel.getTravel().removeAllObjects()
        ObjectFavorites.getData().removeAllObjects()
        UserDefaults.standard.synchronize()
        
        if labelPriceTravel != nil
        {
            self.labelPriceTravel.text = "Elija sus lugares de inicio y de destino para calcular su tarifa de viaje"
        }

        
        if initTravel != nil
        {
            self.initTravel.text = ""
            self.finishTravel.text = ""
            self.initTravel.isEnabled = true
            self.finishTravel.isEnabled = true
        }

    }
}

extension MapViewController: PlacesPickerDelegate {
    func placePickerControllerDidCancel(controller: PlacePickerController) {
        controller.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func placePickerController(controller: PlacePickerController, didSelectPlace place: GMSPlace) {
        controller.navigationController?.dismiss(animated: true, completion: nil)
        print(place.name)
        print(place.coordinate)
        
        if typeTextField == "finishTravel"
        {
            
            if place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n") == ""
            {
                finishTravel.text =  place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n")
            }
            else
            {
                finishTravel.text = place.name
            }
            latitudeFinish = place.coordinate.latitude
            longitudeFinish = place.coordinate.longitude
            addMarkerInMap(lat: place.coordinate.latitude, long: place.coordinate.longitude, title: "Destino", snippet: "")
            let loc : CLLocation = CLLocation(latitude: latitudeFinish, longitude: longitudeFinish)
            updateMapFrame(newLocation: loc, zoom: 15.0)
        }
        else
        {
            if place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n") == ""
            {
                initTravel.text =  place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n")
            }
            else
            {
                initTravel.text =   place.name
            }
            latitudeInit = place.coordinate.latitude
            longitudeInit = place.coordinate.longitude
            addMarkerInMap(lat: latitudeInit , long: longitudeInit , title: "Inicio", snippet: "")
            let loc : CLLocation = CLLocation(latitude: latitudeInit, longitude: longitudeInit)
            updateMapFrame(newLocation: loc, zoom: 15.0)
        }
        
        if ((initTravel.text != "") && (finishTravel.text != ""))
        {
            labelPriceTravel.text = "Calculando datos de viaje"
            DispatchQueue.main.async {
                self.calculatePriceRoute(latitudeInit: self.latitudeInit, longitudeInit: self.longitudeInit, latitudeFinish: self.latitudeFinish, longitudeFinish: self.longitudeFinish)
            }
        }
        
        
        //viewController.dismiss(animated: true, completion: nil)
        
        //self.lblName.text = place.name
        //self.lblAddress.text = place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n")
        //self.lblLatitude.text = String(place.coordinate.latitude)
        //self.lblLongitude.text = String(place.coordinate.longitude)
    }
}




//marker move on map view
//    func playCar()
//    {
//
//        if iTemp <= (objMapModel.arrayMapPath.count - 1 )
//        {
//            let iTempMapPath = objMapModel.arrayMapPath[iTemp]
//
//            let loc : CLLocation = CLLocation(latitude: iTempMapPath.lat!, longitude: iTempMapPath.lon!)
//
//
//            //self.updateMapFrame(newLocation: loc, zoom: self.mapView.camera.zoom)
//
//            marker.position = CLLocationCoordinate2DMake(iTempMapPath.lat!, iTempMapPath.lon!)
//
//            marker.rotation = iTempMapPath.angle!
//
//            marker.icon = UIImage(named: "map_car_running.png")
//            marker.map = mapView
//
//            // Timer close
//            if iTemp == (objMapModel.arrayMapPath.count - 1)
//            {
//                // timer close
//                timer.invalidate()
//                showFinishTravel()
//                //buttonPlay.isEnabled = true
//                iTemp = 0
//            }
//            iTemp += 1
//        }
//    }


//    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
//        let customInfoWindow = Bundle.main.loadNibNamed("MapInfoWindowRView", owner: self, options: nil)![0] as! MapInfoWindowRView
//        customInfoWindow.mTitleLabel.text = marker.title
//        customInfoWindow.mAddressLabel.text = marker.snippet
//
//        return customInfoWindow
//    }


