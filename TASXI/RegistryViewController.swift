//
//  RegistryViewController.swift
//  TASXI
//
//  Created by Josué :D on 10/05/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import PKHUD
import Firebase
import FirebaseMessaging

class RegistryViewController: UIViewController,UITextFieldDelegate {
    
    var first_name = ""
    var last_name = ""
    var email = ""
    var idClass = ""
    
    var fbRegister = "false"
    
    @IBOutlet var fieldFirstName: CustomTextField!
    @IBOutlet var fieldLastName: CustomTextField!
    @IBOutlet var fieldGender: CustomTextField!
    @IBOutlet var fieldAge: CustomTextField!
    @IBOutlet var fieldEmail: CustomTextField!
    @IBOutlet var fieldTelephone: CustomTextField!
    @IBOutlet var fieldPassword: CustomTextField!
    
    @IBOutlet var buttonRegister: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
        self.navigationItem.title = "REGISTRO"
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fieldFirstName.text = first_name
        fieldLastName.text = last_name
        fieldEmail.text = email

        fieldAge.inputView = UIView()
        fieldAge.delegate = self
        
        fieldGender.inputView = UIView()
        fieldGender.delegate = self
        
        buttonRegister.addTarget(self, action: #selector(self.register), for: UIControlEvents.touchUpInside)
        
        if fbRegister == "true"
        {
           verifyFbLogin()
        }

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == fieldAge {
            
            var array = [String]()
            for i in 18...100 {
                array.append(String(i))
            }
            
            DPPickerManager.shared.showPicker(title: "Edad", selected: "18", strings: array) { (value, index, cancel) in
                if !cancel {
                    // TODO: you code here
                    debugPrint(value as Any)
                    self.fieldAge.text = value
                }
            }
        
            return false;
        }
        
        if textField == fieldGender {
            
            let values = ["Masculino", "Femenino"]
            DPPickerManager.shared.showPicker(title: "Sexo", selected: "Masculino", strings: values) { (value, index, cancel) in
                if !cancel {
                    // TODO: you code here
                    debugPrint(value as Any)
                    self.fieldGender.text = value
                }
            }
            
            return false;
        }
        
        return true
    }
    
    @objc func register()
    {

        if ((fieldFirstName.text != "") && (fieldLastName.text != "")  && (fieldGender.text != "")  && (fieldAge.text != "")  && (fieldEmail.text != "")  && (fieldTelephone.text != "")  && (fieldPassword.text != "") )
        {
            registerUser()
        }
        else
        {
            let alert = UIAlertController(title: "Taxsi", message: "Por favor no deje campos de texto vacíos", preferredStyle: .alert)
            let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
            alert.addAction(actionOK)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func registerUser()
    {
        
//        @IBOutlet var fieldFirstName: CustomTextField!
//        @IBOutlet var fieldLastName: CustomTextField!
//        @IBOutlet var fieldGender: CustomTextField!
//        @IBOutlet var fieldAge: CustomTextField!
//        @IBOutlet var fieldEmail: CustomTextField!
//        @IBOutlet var fieldTelephone: CustomTextField!
//        @IBOutlet var fieldPassword: CustomTextField!
        
        HUD.show(.progress)
        
        var typeRegister = "1"
        if fbRegister == "true"
        {
            typeRegister = "0"
        }
        
        
        var gender = "M"
        if fieldGender.text! == "Masculino"
        {
            gender = "H"
        }
        

        let parameters: Parameters = [
            "tipo": typeRegister,
            "nombre": fieldFirstName.text! as Any,
            "apellidos": fieldLastName.text! as Any,
            "sexo": gender,
            "email": fieldEmail.text! as Any,
            "password": fieldPassword.text! as Any,
            "telefono": fieldTelephone.text! as Any,
            "edad": fieldAge.text! as Any ,
            "foto": ""
        ]
        
        print(parameters)
        Alamofire.request(ApiDefinition.WS_LOGIN, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
   
                print("JSON: \(json)")
                let pasajero = json["pasajero"]
                let id = pasajero[0]["id"].string!
                if id != "0"
                {
                    HUD.flash(.success, delay: 0.5)
                    
                    let id = pasajero[0]["id"].string!
                    let nombre = pasajero[0]["nombre"].string!
                    let apellidos = pasajero[0]["apellidos"].string!
                    let edad = pasajero[0]["edad"].string!
                    let emailRes = pasajero[0]["email"].string!
                    let sexo = pasajero[0]["sexo"].string!
                    let telefono = pasajero[0]["telefono"].string!
                    
                    UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
                    UserDefaults.standard.setValue(id, forKey: DataPersistent.id)
                    UserDefaults.standard.setValue(nombre, forKey: DataPersistent.nombre)
                    UserDefaults.standard.setValue(edad, forKey: DataPersistent.edad)
                    UserDefaults.standard.setValue(apellidos, forKey: DataPersistent.apellidos)
                    UserDefaults.standard.setValue(emailRes, forKey: DataPersistent.email)
                    UserDefaults.standard.setValue(sexo, forKey: DataPersistent.sexo)
                    UserDefaults.standard.setValue("", forKey: DataPersistent.imageUrl)
                    UserDefaults.standard.setValue(telefono, forKey: DataPersistent.telefono)
                    UserDefaults.standard.synchronize()
                    
                    let token = Messaging.messaging().fcmToken
                    print("FCM token: \(token ?? "")")
                    
                    Messaging.messaging().subscribe(toTopic: "c\(id)") { error in
                        print("Subscribed to c\(id) topic")
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let pushController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
                        self.navigationController?.pushViewController(pushController, animated: true)

                    }
                    
                }
                else
                {
                    HUD.flash(.error, delay: 0.5)
                }
                
            case .failure(let error):
                print(error)
                HUD.flash(.error, delay: 0.5)
            }
        }
    }
    
    
    func verifyFbLogin()
    {
        
       HUD.show(.progress)
        
       let typeRegister = "0"

        print(self.email)
        //let uEmail = String(describing: UserDefaults.standard.value(forKey: self.email)!)
        
        let parameters: Parameters = [
            "tipo": typeRegister,
            "email": self.email,
        ]
        
        print(parameters)
        Alamofire.request(ApiDefinition.WS_LOGIN, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON: \(json)")
                if json != nil
                {
                    let pasajero = json["pasajero"]
                    let id = pasajero[0]["id"].string!
                    if id != "0"
                    {
                        HUD.flash(.success, delay: 0.5)
                        
                        let id = pasajero[0]["id"].string!
                        let nombre = pasajero[0]["nombre"].string!
                        let apellidos = pasajero[0]["apellidos"].string!
                        let edad = pasajero[0]["edad"].string!
                        let emailRes = pasajero[0]["email"].string!
                        let sexo = pasajero[0]["sexo"].string!
                        let telefono = pasajero[0]["telefono"].string!
                        
                        let imageFb = "https://graph.facebook.com/\(self.idClass)/picture?type=large"
                        print(imageFb)
                        
                        UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
                        UserDefaults.standard.setValue(id, forKey: DataPersistent.id)
                        UserDefaults.standard.setValue(nombre, forKey: DataPersistent.nombre)
                        UserDefaults.standard.setValue(edad, forKey: DataPersistent.edad)
                        UserDefaults.standard.setValue(apellidos, forKey: DataPersistent.apellidos)
                        UserDefaults.standard.setValue(emailRes, forKey: DataPersistent.email)
                        UserDefaults.standard.setValue(sexo, forKey: DataPersistent.sexo)
                        UserDefaults.standard.setValue(imageFb, forKey: DataPersistent.imageUrl)
                        UserDefaults.standard.setValue(telefono, forKey: DataPersistent.telefono)
                        UserDefaults.standard.synchronize()
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let pushController = storyboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
                            self.navigationController?.pushViewController(pushController, animated: true)
                        }
                    }
                    else
                    {
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        }
                    }
                }
                else
                {
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                    }
                }
            case .failure(let error):
                print(error)
                PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
         
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
