//
//  FavoritePlaces.swift
//  TASXI
//
//  Created by Josue Hernandez on 9/14/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import PKHUD
import GoogleMaps
import GooglePlaces
import GooglePlacePicker

class FavoritePlaces: UIViewController, UITableViewDelegate, UITableViewDataSource, GMSMapViewDelegate,MapPathViewModelDelegate,UITextFieldDelegate {

    
    @IBOutlet var tableView: UITableView!
    
    
    private var data: [String] = []
    var latitude = 0.0
    var longitude = 0.0
    var name_place = ""
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
        self.navigationItem.title = "FAVORITOS"
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
        //
        //        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "menubar.png"), style: .done, target: self, action: #selector(closeMenu))
        //        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        var image = UIImage(named: "add")
        image?.resizeImage(targetSize: CGSize(width: 25.0, height: 25.0))
        image = image?.withRenderingMode(.alwaysOriginal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style:.plain, target: self, action: #selector(self.getPlacePickerViewNew))
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        //tableView.separatorColor = UIColor.white
        
        data.append("Historial de viajes")
        data.append("Configuraciòn de cuenta")
        data.append("Mis favoritos")
        data.append("Cerrar sesión")
        
        //requestCancelTravel()
        //getFavorites()
        
        let key = "AIzaSyAeWZI4i1auhjZ280PQ9UolktKqr8SswyQ"
        PlacePicker.configure(googleMapsAPIKey: key, placesAPIKey: key)
        
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.getFavorites()

        })
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //self.getFavorites()
        //        pickTabla.showPickerr(ancho: self.finishTravel.frame.width , viewcontroller: self, x: finishTravel.frame.origin.x, y: finishTravel.frame.origin.y, boton: buttonFinishTravel)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ObjectFavorites.getData().count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCellFavorites
        
        
        let object : ObjectFavorites = ObjectFavorites.getData().object(at: indexPath.row) as! ObjectFavorites
        
        cell.labelText.text = object.nombre_lugar
//        cell.destinationTravel.adjustsFontSizeToFitWidth = true
//        cell.date.adjustsFontSizeToFitWidth = true
//
//        cell.date.text = object.fecha_solicitud
//        cell.car.text = object.modelo
//        cell.startTravel.text = object.lugar_partida
//        cell.destinationTravel.text = object.lugar_llegada
//        cell.nameDriver.text = object.nombre_conductor
//
//        //cell.label.text = text
//
//        if (object.calificacion_conductor == "null")
//        {
//            cell.starsPoints.text = "?"
//        }
//        else
//        {
//            //
//            //cell.starsPoints.text = Double(object.calificacion_conductor )
//            var stars = object.calificacion_conductor
//            cell.starsPoints.text = String((stars as NSString).doubleValue)
//        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true);
        print(indexPath.row)
        
        
        
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let pushController = storyboard.instantiateViewController(withIdentifier: "detalle") as! Detalle
        //        self.navigationController?.pushViewController(pushController, animated: true)
        //
        
    }
    
    func addPlaceFavorite()
    {
        
    }
    
    @objc func getPlacePickerView() {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
    
    @objc func getPlacePickerViewNew()
    {
        typeTextField = "favorito"
        let controller = PlacePicker.placePickerController()
        controller.delegate = self
        let navigationController = UINavigationController(rootViewController: controller)
        self.show(navigationController, sender: nil)
    }
    
}

extension FavoritePlaces : GMSPlacePickerViewControllerDelegate
{
    // GMSPlacePickerViewControllerDelegate and implement this code.
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        //self.viewContainer.isHidden = false
        //self.indicatorView.isHidden = true
        //viewController.title = "Easkj kjs"
        //viewController.navigationItem.title = "INICdffsfIO"

            latitude = place.coordinate.latitude
          longitude = place.coordinate.longitude

        
        viewController.dismiss(animated: true, completion: nil)
        
        let alert = UIAlertController(title: "Taxsi", message: "Agrege el nombre de su lugar favorito", preferredStyle:
            UIAlertControllerStyle.alert)
        
        alert.addTextField { (textField) in
            //textField.text = "Some default text"
        }
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,  handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            print("Text field: \(textField?.text)")
            self.name_place = (textField?.text!)!
            
            self.addFavorite()
        }))
        
        self.present(alert, animated: true, completion:nil)
        
        //self.lblName.text = place.name
        //self.lblAddress.text = place.formattedAddress?.components(separatedBy: ", ").joined(separator: "\n")
        //self.lblLatitude.text = String(place.coordinate.latitude)
        //self.lblLongitude.text = String(place.coordinate.longitude)
    }
    

    
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        
        viewController.dismiss(animated: true, completion: nil)
        
        //self.viewContainer.isHidden = true
        // self.indicatorView.isHidden = true
    }
    
    func getFavorites()
    {
        DispatchQueue.main.async {
            HUD.show(.progress)
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
            let parameters: Parameters = [
                "id_pasajero" : id
            ]
            print("WS_GET_FAVORITES")
            print("\(ApiDefinition.WS_GET_FAVORITES)?id_pasajero=\(id)")
            Alamofire.request(ApiDefinition.WS_GET_FAVORITES, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    
                    print("WS_GET_FAVORITES: \(json)")
                    let resultado = json["resultado"]
                    let res = resultado[0]["resultado"]
                    if res != 0
                    {
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            
                            
                            ObjectFavorites.getData().removeAllObjects()
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            let lugar = json["lugar"]
                            
                            for index in 0..<(Int(lugar.array!.count)) {
                                let latitud = String(describing:lugar[index]["latitud"])
                                let longitud = String(describing:lugar[index]["longitud"])
                                let id = String(describing:lugar[index]["id"])
                                let nombre_lugar = String(describing:lugar[index]["nombre_lugar"])
                                print(nombre_lugar)
                                
                                
                                let object =  ObjectFavorites(id: id, latitud: latitud, longitud: longitud, nombre_lugar: nombre_lugar)
                                
                                
                                ObjectFavorites.getData().add(object)
                                
                            }
                            
                            self.tableView.reloadData()
                            
                            
                        }
                    }
                    else
                    {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                            //                        let alert = UIAlertController(title: "Taxsi", message: "No se han podido cargar sus favoritos", preferredStyle: .alert)
                            //                        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                            //                        alert.addAction(actionOK)
                            //                        self.present(alert, animated: true, completion: nil)
                        }
                    }
                case .failure(let error):
                    print(error)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
        }
    }
    
    func addFavorite()
    {
        HUD.show(.progress)
        
        
        let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        let id_viaje = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
        
        let parameters: Parameters = [
            "id_pasajero": id,
            "nombre_lugar" : name_place,
            "latitud" : latitude ,
            "longitud" : longitude ,
            ]
        
        print("WS_ADD_FAVORITE")
        print("\(ApiDefinition.WS_ADD_FAVORITE)?id_pasajero=\(id)&nombre_lugar=\(name_place)&latitud=\(latitude)&longitud=\(longitude)")
        
        Alamofire.request(ApiDefinition.WS_ADD_FAVORITE, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                print("WS_ADD_FAVORITE: \(json)")
                let resultado = json["resultado"]
                let res = resultado[0]["resultado"]
                if res != 0
                {
                    
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        
                        
                        ObjectFavorites.getData().removeAllObjects()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let lugar = json["lugar"]
                        
                        for index in 0..<(Int(lugar.array!.count)) {
                            let latitud = String(describing:lugar[index]["latitud"])
                            let longitud = String(describing:lugar[index]["longitud"])
                            let id = String(describing:lugar[index]["id"])
                            let nombre_lugar = String(describing:lugar[index]["nombre_lugar"])
                            print(nombre_lugar)
                            
                            
                            let object =  ObjectFavorites(id: id, latitud: latitud, longitud: longitud, nombre_lugar: nombre_lugar)
                            
                            
                            ObjectFavorites.getData().add(object)
                            
                        }
                        
                        self.tableView.reloadData()
                        HUD.flash(.success, delay: 0.5)
                        
                        
                    }
                    
    
                }
                else
                {
                    
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                         HUD.flash(.error, delay: 0.5)
                    }
                }
            case .failure(let error):
                print(error)
                PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                     HUD.flash(.error, delay: 0.5)
                }
            }
        }
    }
    
    
    func eraseFavorite(indexPath: IndexPath )
    {
        HUD.show(.progress)
        
        let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        
        let object : ObjectFavorites = ObjectFavorites.getData().object(at: indexPath.row) as! ObjectFavorites
        
        let parameters: Parameters = [
            "id_pasajero": id,
            "id_lugar" : object.id
        ]
    
        
        print("WS_ERASE_FAVORITE")
        print("\(ApiDefinition.WS_ERASE_FAVORITE)?id_pasajero=\(id)&id_lugar=\(object.id)")
        
        Alamofire.request(ApiDefinition.WS_ERASE_FAVORITE, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                print("WS_ERASE_FAVORITE: \(json)")
                let resultado = json["resultado"]
                let res = resultado[0]["resultado"]
                if res != 0
                {
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        
                        
                        ObjectFavorites.getData().removeAllObjects()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let lugar = json["lugar"]
                        
                        if lugar.count != 0
                        {
                            for index in 0..<(Int(lugar.array!.count)) {
                                let latitud = String(describing:lugar[index]["latitud"])
                                let longitud = String(describing:lugar[index]["longitud"])
                                let id = String(describing:lugar[index]["id"])
                                let nombre_lugar = String(describing:lugar[index]["nombre_lugar"])
                                print(nombre_lugar)
                                
                                
                                let object =  ObjectFavorites(id: id, latitud: latitud, longitud: longitud, nombre_lugar: nombre_lugar)
                                
                                
                                ObjectFavorites.getData().add(object)
                                
                            }
                        }
 
                        
                        self.tableView.reloadData()
                        HUD.flash(.success, delay: 0.5)

                    }
                }
                else
                {
                    
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        HUD.flash(.error, delay: 0.5)
                        //self.tableView.beginUpdates()
                        //self.tableView.endUpdates()
                    }
                }
            case .failure(let error):
                print(error)
                PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                     HUD.flash(.error, delay: 0.5)
                    //self.tableView.beginUpdates()
                    //self.tableView.endUpdates()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle:   UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            eraseFavorite(indexPath: indexPath)
        }
    }
    
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String?
    {
        return "Borrar"
    }
    

    
  
    
 
}

// TRACK ROUTE
extension FavoritePlaces{
    
    //Success json read delegate method
    func isSucessReadJson()  {
        //drawPathOnMap()
    }
    
    //fail json read delegate method
    func isFailReadJson(msg : String)  {
        let alert = UIAlertController(title: "Map Alert", message: msg, preferredStyle: .alert)
        let actionOK : UIAlertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(actionOK)
        self.present(alert, animated: true, completion: nil)
    }
}

extension UIImage {
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        let newSize = widthRatio > heightRatio ?  CGSize(width: size.width * heightRatio, height: size.height * heightRatio) : CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}



extension FavoritePlaces: PlacesPickerDelegate {
    func placePickerControllerDidCancel(controller: PlacePickerController) {
        controller.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func placePickerController(controller: PlacePickerController, didSelectPlace place: GMSPlace) {
        controller.navigationController?.dismiss(animated: true, completion: nil)
        print(place.name)
        print(place.coordinate)
        
        DispatchQueue.main.async {
            
            self.latitude = place.coordinate.latitude
            self.longitude = place.coordinate.longitude
            
            
            let alert = UIAlertController(title: "Taxsi", message: "Agrege el nombre de su lugar favorito", preferredStyle:
                UIAlertControllerStyle.alert)
            
            alert.addTextField { (textField) in
                //textField.text = "Some default text"
            }
            
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default,  handler: { [weak alert] (_) in
                let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                print("Text field: \(textField?.text)")
                self.name_place = (textField?.text!)!
                
                self.addFavorite()
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        
        
    }
}

