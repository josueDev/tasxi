//
//  HistoryTravelViewController.swift
//  TASXI
//
//  Created by Josué :D on 11/05/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON
import PKHUD

class HistoryTravelViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var tableView: UITableView!
    
     private var data: [String] = []
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
        self.navigationItem.title = "HISTORIAL DE VIAJES"
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedStringKey.foregroundColor: UIColor(red: 7/255, green: 7/255, blue: 7/255, alpha: 1)] // titleBar
//
//        let leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "menubar.png"), style: .done, target: self, action: #selector(closeMenu))
//        self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        //tableView.separatorColor = UIColor.white
        
        data.append("Historial de viajes")
        data.append("Configuraciòn de cuenta")
        data.append("Mis favoritos")
        data.append("Cerrar sesión")
        
        requestCancelTravel()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ObjectHistoryTravel.getData().count
        
    }
    
    func tableView(_ tableView: UITableView,heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 188
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCellHistoryTravel
        
       
        let object : ObjectHistoryTravel = ObjectHistoryTravel.getData().object(at: indexPath.row) as! ObjectHistoryTravel
        
        cell.startTravel.adjustsFontSizeToFitWidth = true
        cell.destinationTravel.adjustsFontSizeToFitWidth = true
        cell.date.adjustsFontSizeToFitWidth = true
        cell.nameDriver.adjustsFontSizeToFitWidth = true
        
        cell.date.text = object.fecha_solicitud
        cell.startTravel.text = object.lugar_partida
        cell.destinationTravel.text = object.lugar_llegada
        //cell.nameDriver.text = object.nombre_conductor
       
        //cell.label.text = text
        
        if (object.calificacion_conductor == "null")
        {
            cell.starsPoints.text = "-"
        }
        else
        {
            //
            //cell.starsPoints.text = Double(object.calificacion_conductor )
            var stars = object.calificacion_conductor
            cell.starsPoints.text = String((stars as NSString).doubleValue)
        }
        
        if (object.modelo == "null")
        {
            cell.car.text = "Viaje cancelado"
            cell.car.textColor = UIColor.red
        }
        else
        {
            cell.car.text = object.modelo
            cell.car.textColor = UIColor.black
        }
        
        if (object.nombre_conductor == " ")
        {
            cell.nameDriver.text = "Chofer no asignado"
            //cell.nombre_conductor.textColor = UIColor.red
        }
        else
        {
            cell.nameDriver.text = object.nombre_conductor
            //cell.nombre_conductor.textColor = UIColor.black
        }
        
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true);
        print(indexPath.row)
        
        
        
        //        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //        let pushController = storyboard.instantiateViewController(withIdentifier: "detalle") as! Detalle
        //        self.navigationController?.pushViewController(pushController, animated: true)
        //
        
    }
    
    func requestCancelTravel()
    {
        HUD.show(.progress)
        
        let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        let id_viaje = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
        
        let parameters: Parameters = [
            "id_pasajero": id,
        ]
        
        print("WS_GET_HISTORY_TRAVEL")
        print("\(ApiDefinition.WS_GET_HISTORY_TRAVEL)?id_pasajero=\(id)")
        
        Alamofire.request(ApiDefinition.WS_GET_HISTORY_TRAVEL, method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                print("WS_GET_HISTORY_TRAVEL: \(json)")
                let resultado = json["resultado"]
                let res = resultado[0]["resultado"]
                if res != 0
                {
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        

                        ObjectHistoryTravel.getData().removeAllObjects()
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        let viaje = json["viaje"]
                        
                        for index in 0..<(Int(viaje.array!.count)) {
                            let id = String(describing:viaje[index]["id"])
                            let id_pasajero = String(describing:viaje[index]["id_pasajero"])
                            let id_conductor = String(describing:viaje[index]["id_conductor"])
                            let nombre_pasajero = String(describing:viaje[index]["nombre_pasajero"])
                            let nombre_conductor = String(describing:viaje[index]["nombre_conductor"])
                            let foto_conductor = String(describing:viaje[index]["foto_conductor"])
                            let unidad = String(describing:viaje[index]["unidad"])
                            let modelo = String(describing:viaje[index]["modelo"])
                            let placas = String(describing:viaje[index]["placas"])
                            let fecha_solicitud = String(describing:viaje[index]["fecha_solicitud"])
                            let fecha_inicio = String(describing:viaje[index]["fecha_inicio"])
                            let fecha_llegada = String(describing:viaje[index]["fecha_llegada"])
                            let lugar_partida = String(describing:viaje[index]["lugar_partida"])
                            let lugar_llegada = String(describing:viaje[index]["lugar_llegada"])
                            let calificacion_conductor = String(describing:viaje[index]["calificacion_conductor"])
                            let comentario_a_conductor = String(describing:viaje[index]["comentario_a_conductor"])
                            let calificacion_pasajero = String(describing:viaje[index]["calificacion_pasajero"])
                            let comentario_a_pasajero = String(describing:viaje[index]["comentario_a_pasajero"])
                            let estatus = String(describing:viaje[index]["estatus"])
                            
                            
                            let object = ObjectHistoryTravel(id: id, id_pasajero: id_pasajero, id_conductor: id_conductor, nombre_pasajero: nombre_pasajero, nombre_conductor: nombre_conductor, foto_conductor: foto_conductor, unidad: unidad, modelo: modelo, placas: placas, fecha_solicitud: fecha_solicitud, fecha_inicio: fecha_inicio, fecha_llegada: fecha_llegada, lugar_partida: lugar_partida, lugar_llegada: lugar_llegada, calificacion_conductor: calificacion_conductor, comentario_a_conductor: comentario_a_conductor, calificacion_pasajero: calificacion_pasajero, comentario_a_pasajero: comentario_a_pasajero, estatus: estatus)
                            
                            print(id)
                            ObjectHistoryTravel.getData().add(object)
                            
                        }
                        
                        self.tableView.reloadData()
                        

                    }
                }
                else
                {
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        let alert = UIAlertController(title: "Taxsi", message: "No se ha podido cargar los viajes", preferredStyle: .alert)
                        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                        alert.addAction(actionOK)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            case .failure(let error):
                print(error)
                PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                    
                    let alert = UIAlertController(title: "Taxsi", message: "No se ha podido cargar los viajes", preferredStyle: .alert)
                    let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                    alert.addAction(actionOK)
                    self.present(alert, animated: true, completion: nil)
            
                }
            }
        }
    }
}
