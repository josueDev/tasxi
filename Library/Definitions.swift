//
//  Definitions.swift
//  x24
//
//  Created by Charls Salazar on 13/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation

class ApiDefinition: NSObject {

    //static let API_SERVER = "http://sinergiadigital.com.mx"
    static let API_SERVER = "https://taxsi.mx"
    static let WS_LOGIN = API_SERVER + "/servicios/autenticaPasajero.php"
    static let WS_REQUEST_TRAVEL = API_SERVER +  "/servicios/solicitaViaje.php"
    static let WS_REQUEST_TRAVEL_2 = API_SERVER +  "/servicios/solicitaViaje2.php"
    static let WS_STATUS_TRAVEL = API_SERVER +  "/servicios/getEstatusPasajero.php"
    static let WS_EVALUATE_DRIVER = API_SERVER +  "/servicios/evaluaConductor.php"
    static let WS_PENDING_EVALUATE_DRIVER = API_SERVER +  "/servicios/pasajeroTieneCalifPendiente.php"
    static let WS_UPDATE_PASSENGER = API_SERVER +  "/servicios/updatePasajero.php"
    static let WS_UPDATE_PASSWORD = API_SERVER +  "/servicios/updatePasswordPasajero.php"
    static let WS_DATA_TRAVEL = API_SERVER + "/servicios/getDatosViajePasajero.php"
    static let WS_CANCEL_TRAVEL = API_SERVER + "/servicios/cancelaViaje.php"
    static let WS_GET_COOR_DRIVER = API_SERVER + "/servicios/getCoordenadasViaje.php"
    static let WS_GET_HISTORY_TRAVEL = API_SERVER + "/servicios/getHistoricoViajesPasajero.php"
    static let WS_CALCULATE_PRICE = API_SERVER + "/servicios/calcularTarifa.php"
    static let WS_ADD_FAVORITE = API_SERVER + "/servicios/agregaLugar.php"
    static let WS_ERASE_FAVORITE = API_SERVER + "/servicios/eliminaLugar.php"
    static let WS_GET_FAVORITES = API_SERVER + "/servicios/getLugaresPasajero.php"
    static let WS_CANCEL_TRAVELS = API_SERVER + "/servicios/cancelaViajesPendientesPasajero.php"
    static let WS_TEL = API_SERVER + "/servicios/getTelefonoCentral.php"
    
    
    

    
}
