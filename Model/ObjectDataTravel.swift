//
//  ObjectDataTravel.swift
//  TASXI
//
//  Created by Josué :D on 28/07/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

private var array: NSMutableArray = NSMutableArray()

class ObjectDataTravel: NSObject {
    var fecha_solicitud: String
    var estatus : String
    var latitud_partida : String
    var latitud_llegada_sol : String
    var longitud_llegada_real : String
    var nombre_conductor : String
    var fecha_llegada : String
    var longitud_partida : String
    var id_conductor: String
    var id : String
    var fecha_inicio : String
    var tel_conductor : String
    var nombre_pasajero : String
    var foto_conductor : String
    var id_pasajero : String
    var correo_conductor : String
    var placas : String
    var latitud_llegada_real : String
    var tel_pasajero: String
    var lugar_llegada : String
    var unidad : String
    var longitud_llegada_sol : String
    var correo_pasajero : String
    var longitud_conductor : String
    var lugar_partida : String
    var modelo : String
    var latitud_conductor : String
    var promedio_conductor : String
    
    
    init( fecha_solicitud : String,estatus : String, latitud_partida : String, latitud_llegada_sol : String, longitud_llegada_real : String, nombre_conductor : String,fecha_llegada : String, distance : String,longitud_partida : String,id_conductor : String, id : String, fecha_inicio : String, tel_conductor : String, nombre_pasajero : String, foto_conductor : String,id_pasajero : String, correo_conductor : String,placas : String,latitud_llegada_real : String, tel_pasajero : String, lugar_llegada : String, unidad : String, longitud_llegada_sol : String,correo_pasajero : String, longitud_conductor : String,lugar_partida : String,modelo : String, latitud_conductor : String , promedio_conductor : String) {
        
        
        self.fecha_solicitud = fecha_solicitud
        self.estatus = estatus
        self.latitud_partida = latitud_partida
        self.latitud_llegada_sol = latitud_llegada_sol
        self.longitud_llegada_real = longitud_llegada_real
        self.nombre_conductor = nombre_conductor
        self.fecha_llegada = fecha_llegada
        self.longitud_partida = longitud_partida
        self.id_conductor = id_conductor
        self.id = id
        self.fecha_inicio = fecha_inicio
        self.tel_conductor = tel_conductor
        self.nombre_pasajero = nombre_pasajero
        self.foto_conductor = foto_conductor
        self.id_pasajero = id_pasajero
        self.correo_conductor = correo_conductor
        self.placas = placas
        self.latitud_llegada_real = latitud_llegada_real
        self.tel_pasajero = tel_pasajero
        self.lugar_llegada = lugar_llegada
        self.unidad = unidad
        self.longitud_llegada_sol = longitud_llegada_sol
        self.correo_pasajero = correo_pasajero
        self.longitud_conductor = longitud_conductor
        self.lugar_partida = lugar_partida
        self.modelo = modelo
        self.latitud_conductor = latitud_conductor
        self.promedio_conductor = promedio_conductor
    
    }
    class func getData() -> NSMutableArray {
        return array
    }
    
    class func setData( listac : NSMutableArray ) {
        array = listac
    }
}
