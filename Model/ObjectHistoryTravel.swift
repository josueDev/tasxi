//
//  ObjectHistoryTravel.swift
//  TASXI
//
//  Created by Josué :D on 26/08/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//


import UIKit
import MapKit
import CoreLocation

private var array: NSMutableArray = NSMutableArray()

class ObjectHistoryTravel: NSObject {
    
    var id: String
    var id_pasajero : String
    var id_conductor : String
    var nombre_pasajero : String
    var nombre_conductor : String
    var foto_conductor: String
    var unidad: String
    var modelo: String
    var placas: String
    var fecha_solicitud: String
    var fecha_inicio: String
    var fecha_llegada: String
    var lugar_partida: String
    var lugar_llegada: String
    var calificacion_conductor: String
    var comentario_a_conductor: String
    var calificacion_pasajero: String
    var comentario_a_pasajero: String
    var estatus: String
    
    
    
    init( id : String,id_pasajero : String, id_conductor : String, nombre_pasajero : String, nombre_conductor : String, foto_conductor : String,unidad : String, modelo : String,placas : String,fecha_solicitud : String, fecha_inicio : String, fecha_llegada : String, lugar_partida : String, lugar_llegada : String, calificacion_conductor : String,comentario_a_conductor : String, calificacion_pasajero : String,comentario_a_pasajero : String,estatus : String) {
        
        
        self.id = id
        self.id_pasajero = id_pasajero
        self.id_conductor = id_conductor
        self.nombre_pasajero = nombre_pasajero
        self.nombre_conductor = nombre_conductor
        self.foto_conductor = foto_conductor
        self.unidad = unidad
        self.modelo = modelo
        self.placas = placas
        self.fecha_solicitud = fecha_solicitud
        self.fecha_inicio = fecha_inicio
        self.fecha_llegada = fecha_llegada
        self.lugar_partida = lugar_partida
        self.lugar_llegada = lugar_llegada
        self.calificacion_conductor = calificacion_conductor
        self.comentario_a_conductor = comentario_a_conductor
        self.calificacion_pasajero = calificacion_pasajero
        self.comentario_a_pasajero = comentario_a_pasajero
        self.estatus = estatus
        
    }
    class func getData() -> NSMutableArray {
        return array
    }
    
    class func setData( listac : NSMutableArray ) {
        array = listac
    }
}

