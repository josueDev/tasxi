//
//  ObjectFavorites.swift
//  TASXI
//
//  Created by Josue Hernandez on 9/14/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//


import UIKit
import MapKit
import CoreLocation

private var array: NSMutableArray = NSMutableArray()

class ObjectFavorites: NSObject {
    
    var latitud: String
    var longitud : String
    var id : String
    var nombre_lugar : String
    
    
    init( id : String,latitud : String, longitud : String, nombre_lugar : String) {
        
        self.latitud = latitud
        self.longitud = longitud
        self.id = id
        self.nombre_lugar = nombre_lugar

    }
    class func getData() -> NSMutableArray {
        return array
    }
    
    class func setData( listac : NSMutableArray ) {
        array = listac
    }
}
